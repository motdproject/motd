﻿using UnityEngine;
using System.Collections;

public class Tower : Photon.MonoBehaviour
{
	public string id;
	public int price;
	public int buildDuration;
	public float radius;

	private SphereCollider _collider;
			
	void Awake ()
	{
		_collider = GetComponent<SphereCollider> ();
		if (!_collider) {
			_collider = GetComponentInChildren<SphereCollider> ();
		}
		_collider.radius = radius;
	}
}
