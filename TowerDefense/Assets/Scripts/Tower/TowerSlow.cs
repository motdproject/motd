﻿using UnityEngine;
using System.Collections;

public class TowerSlow : Tower
{
	void OnTriggerEnter (Collider other)
	{
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.ENEMY) {
			SlowEnemy (other.gameObject);
		}
	}
	
	void OnTriggerStay (Collider other)
	{
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.ENEMY) {
			SlowEnemy (other.gameObject);
		}
	}
	
	void OnTriggerExit (Collider other)
	{
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.ENEMY) {
			UnSlowEnemy (other.gameObject);
		}
	}
	
	void SlowEnemy (GameObject enemy)
	{
		EnemyMove move = enemy.GetComponent <EnemyMove> ();
		move.Slow ();
	}
	
	void UnSlowEnemy (GameObject enemy)
	{
		EnemyMove move = enemy.GetComponent <EnemyMove> ();
		move.UnSlow ();
	}
}
