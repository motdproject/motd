﻿using UnityEngine;
using System.Collections;

public class TowerAttack : Tower
{
	public GameObject animationObject;
	public float animationDuration;
	public float attackSpeed;
	public float attackStrength;

	private float _lastAttackTime;
	[SerializeField]
	private GameObject
		_target;
	
	void Start ()
	{
		_lastAttackTime = Time.time;
	}
	
	void OnTriggerEnter (Collider other)
	{
		//Debug.Log ("On Trigger Enter");
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.ENEMY) {
			//Debug.Log ("Assigning target?");
			if (!_target) {
				//Debug.Log ("Assigning target");
				_target = other.gameObject;
			}
		} 
	}
	
	void OnTriggerStay (Collider other)
	{
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.ENEMY) {
			if (!_target) {
				_target = other.gameObject;
			}
		}
	}

	void OnTriggerExit (Collider collider)
	{
		if (PhotonNetwork.isMasterClient && collider == _target) {
			_target = null;
		}
	}

	void Update ()
	{
		if (_target) {
			AttackEnemy (_target);
		}
	}
	
	void AttackEnemy (GameObject enemy)
	{
		//Debug.Log ("Attacking target?");
		if (Time.time > _lastAttackTime + attackSpeed) {
			// Show attack animation
			// TODO: attack the enemy rather than the position to account for lag?
			// TODO: show attack over the network
			GetComponent<PhotonView> ().RPC ("AnimateAttack", PhotonTargets.All, enemy.transform.position);
			//Debug.Log ("Attacking target");
			_lastAttackTime = Time.time;
			float damage = attackStrength;
			enemy.GetPhotonView ().RPC ("TakeDamage", PhotonTargets.All, damage);
		} else if (Time.time > _lastAttackTime + animationDuration) {
			animationObject.SetActive (false);
		}
	}

	[RPC]
	void AnimateAttack (Vector3 target)
	{
		GameObject destination = new GameObject ();
		destination.transform.position = target;
		animationObject.SetActive (true);
		animationObject.GetComponent<ElectroScript> ().prefabs.destination = destination.transform;
		Destroy (destination, 1f);
	}
}
