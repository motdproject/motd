﻿using UnityEngine;
using System.Collections;

public class TowerBoost : Tower
{
	public float BoostFactor;

	void OnTriggerEnter (Collider other)
	{
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.PLAYER) {
			Boost (other.gameObject);
		}
	}
	
	void OnTriggerStay (Collider other)
	{
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.PLAYER) {
			Boost (other.gameObject);
		}
	}
	
	void OnTriggerExit (Collider other)
	{
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.PLAYER) {
			UnBoost (other.gameObject);
		}
	}
	
	void Boost (GameObject player)
	{
		PlayerAttack att = player.GetComponent <PlayerAttack> ();
		att.Boost (this.GetHashCode (), BoostFactor);
	}
	
	void UnBoost (GameObject player)
	{
		PlayerAttack att = player.GetComponent <PlayerAttack> ();
		att.UnBoost (this.GetHashCode ());
	}
}
