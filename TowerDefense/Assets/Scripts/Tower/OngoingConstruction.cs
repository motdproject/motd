﻿using UnityEngine;
using System.Collections;

public class OngoingConstruction : MonoBehaviour
{

	public GameObject Tower {
		get {
			return _prefab;
		}
		set {
			_prefab = value;
		}
	}
	public float totalConstructionTime;
	public float alphaStart, alphaEnd;
	
	private Transform _transform;
	private Material _material;
	[SerializeField]
	private GameObject
		_prefab;
	private Color _originalColor;
	private float _startTime, _endTime;

	void Start ()
	{
		_transform = transform;
		if (_prefab) {
			Setup ();
		} else {
			object[] data = GetComponent<PhotonView> ().instantiationData;
			string towerName = data [0] as string;
			totalConstructionTime = (int)(data [1]);
			Tower = Resources.Load (towerName) as GameObject;
			Setup ();
		}
	}

	void Setup ()
	{
		// Get a copy since we'll be doing some modifications and since we can't get .material from a prefab
		GetComponent<MeshRenderer> ().material = Instantiate<Material> (_prefab.GetComponent<MeshRenderer> ().sharedMaterial);
		_material = GetComponent<MeshRenderer> ().material;
		
		GetComponent<MeshFilter> ().mesh = _prefab.GetComponent<MeshFilter> ().sharedMesh;
		GetComponent<MeshCollider> ().sharedMesh = _prefab.GetComponent<MeshCollider> ().sharedMesh;
		
		_transform.localScale = _prefab.transform.localScale;
		
		// Set the material to fade, cf http://forum.unity3d.com/threads/access-rendering-mode-var-on-standard-shader-via-scripting.287002/
		_originalColor = _material.color;
		_material.SwitchRenderModeToFade ();
		
		_startTime = Time.time;
		_endTime = _startTime + totalConstructionTime;
	}

	void Update ()
	{
		float t = (Time.time - _startTime) / _endTime;
		float alpha = Mathf.Lerp (alphaStart / 255, alphaEnd / 255, t);
		Color newColor = new Color (_originalColor.r, _originalColor.g, _originalColor.b, alpha);
		_material.color = newColor;	
		//_material.color = new Color (_material.color.r, _material.color.g, _material.color.b, 0.1f);
	}
}
