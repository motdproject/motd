﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerProxy : MonoBehaviour
{
	public GameObject Tower {
		get {
			return _prefab;
		}
		set {
			_prefab = value;
		}
	}
	public Material constructionOkMaterial;
	public Material constructionKoMaterial;
	public bool canBuild;

	private Camera _mainCamera;
	private Transform _transform;
	private bool _constructionPointChosen;
	private GameObject _prefab;
	private float _printSizeX, _printSizeZ;
	private Grid _grid;

	private List<Collider> blockingColliders = new List<Collider> ();

	void Start ()
	{
		_transform = transform;
		_mainCamera = Camera.main;
		_grid = GameObject.Find ("Map").GetComponent<Grid> ();
		if (_prefab) {
			ComputePrintSize ();
			GetComponent<MeshFilter> ().mesh = Instantiate (_prefab.GetComponent<MeshFilter> ().sharedMesh) as Mesh;
			GetComponent<MeshCollider> ().sharedMesh = Instantiate (_prefab.GetComponent<MeshCollider> ().sharedMesh) as Mesh;

			//GetComponent<MeshRenderer> ().material = constructionOkMaterial;
		}
	}
	
	void Update ()
	{
		if (!_constructionPointChosen) {
			//Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Confined;

			RaycastHit hit;
			Ray ray = _mainCamera.ScreenPointToRay (Input.mousePosition);
		
			// Only if we hit something, do we continue
			if (!Physics.Raycast (_mainCamera.transform.position, ray.direction, out hit, Mathf.Infinity)) {
				return;
			}

			// Align position with the grid
			_transform.position = new Vector3 (hit.point.x, hit.point.y + 0.1f, hit.point.z);
			
			// Get the bottom left corner of the proxy
			Vector3 bottomLeft = new Vector3 (_transform.position.x - _printSizeX / 2, _transform.position.y, transform.position.z - _printSizeZ / 2);
			//Debug.Log ("Bottom left for position " + _transform.position + " is " + bottomLeft);
			// Get the nearest grid point
			Vector3 nearestGridPoint = _grid.GetNearestGridPoint (bottomLeft);
			// Shift the actual position of the proxy
			_transform.position = _transform.position - (bottomLeft - nearestGridPoint); 

			canBuild = CheckBlockingColliders ();

			if (canBuild) {
				// And now look at all the cells below the proxy. If at least one is not tagged as Constructible, 
				// mark the construction as not possible
				// Get all the points from the prefab that are directly above a node of the grid
				List<Vector3> gridPoints = _grid.GetGridPoints (nearestGridPoint, _printSizeX, _printSizeZ);
				
				// Perform a raycast from there to the ground and get the collider
				canBuild = CanBuild (gridPoints);
			}

			if (canBuild) {
				GetComponent<MeshRenderer> ().material = constructionOkMaterial;
			} else {
				GetComponent<MeshRenderer> ().material = constructionKoMaterial;
			}

		}
	}

	void ComputePrintSize ()
	{
		MeshRenderer renderer = _prefab.GetComponent<MeshRenderer> ();
		_printSizeX = renderer.bounds.size.x;
		//_printSizeY = renderer.bounds.size.y;
		_printSizeZ = renderer.bounds.size.z;
		_transform.localScale = _prefab.transform.localScale;
	}

	public void NotifyConstructionChosen ()
	{
		_constructionPointChosen = true;
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Confined;
	}

	public void RemoveProxy ()
	{
		//Debug.Log ("Removing proxy");
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
		Destroy (gameObject);
	}

	bool CheckBlockingColliders ()
	{
		return blockingColliders.Count == 0;
	}

	bool CanBuild (List<Vector3> gridPoints)
	{
		for (int i = 0; i < gridPoints.Count; i++) {
			RaycastHit hit;

			// Should never happen, because that would mean that we're trying to build something 
			// in the empty space
			if (!Physics.Raycast (gridPoints [i] + Vector3.up, Vector3.down, out hit, Mathf.Infinity)) {
				return false;
			}

			if (hit.collider.gameObject.tag != TagNames.CONSTRUCTIBLE) {
				return false;
			}
		}

		return true;
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == TagNames.PLAYER) {
			blockingColliders.Add (other);
		}
	}

	void OnTriggerExit (Collider other)
	{
		if (other.tag == TagNames.PLAYER) {
			blockingColliders.Remove (other);
		}
	}
}
