using UnityEngine;
using System.Collections;

public class SparkScript : MonoBehaviour {
	public AudioClip clip1;
	public AudioClip clip2;
	// Use this for initialization
	void Awake () {
		AudioSource aud = GetComponent<AudioSource>();
		
		if (Random.value >= 0.5f)
			aud.clip = clip1;
		else
			aud.clip = clip2;
	}
	
}
