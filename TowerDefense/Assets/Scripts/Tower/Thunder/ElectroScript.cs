using UnityEngine;
using System.Collections;
using Generic = System.Collections.Generic;

// From ARC LIGHTNING at http://musegames.com/news/general/announcing-the-fabricator-contest-winner-lightning/
public class ElectroScript : MonoBehaviour
{
	public bool attacking = true;

	// Prefabs
	[System.Serializable]
	public class Prefabs
	{
		public LineRenderer lightning;
		public LineRenderer branch;
		public Transform sparks;
		public Transform source;
		public Transform destination;
		public Transform target;
	}
	public Prefabs prefabs;

	// Timing
	[System.Serializable]
	public class Timers
	{
		// Measured in seconds
		public float timeToUpdate = 0.05f;
		public float timeToPowerUp = 0.5f;
		public float branchLife = 0.1f;
	}
	public Timers timers;

	// Dynamics
	[System.Serializable]
	public class Dynamics
	{
		public float chanceToArc = 0.2f;
	}
	public Dynamics dynamics;
	
	// Line Settings
	[System.Serializable]
	public class LineSettings
	{
		public float keyVertexDist = 3.0f;
		public float keyVertexRange = 4.0f;
		public int numInterpoles = 5;
		public float minBranchLength = 11.0f;
		public float maxBranchLength = 16.0f;
	}
	public LineSettings lines;
	
	// Texture Settings
	[System.Serializable]
	public class TextureSettings
	{
		public float scaleX;
		public float scaleY;
		public float animateSpeed;
		public float offsetY;
	}
	public TextureSettings tex;

	int numVertices;
	Vector3 deltaV1;
	Vector3 deltaV2;
	float srcTrgDist;
	float srcDstDist;
	float lastUpdate;
	Hashtable branches;
	
	AudioSource aud;
	
	// Use this for initialization
	void Start ()
	{
		srcTrgDist = 0.0f;
		srcDstDist = 0.0f;
		numVertices = 0;
		deltaV1 = (prefabs.destination.position - prefabs.source.position);
		lastUpdate = 0.0f;
		branches = new Hashtable ();
		aud = GetComponent<AudioSource> ();
	}

	void OnDisable ()
	{
		Debug.Log ("Destroying all artifacts and stopping lightning");
		prefabs.target.position = prefabs.source.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!prefabs.destination) {
			gameObject.SetActive (false);
			return;
		}

		srcTrgDist = Vector3.Distance (prefabs.source.position, prefabs.target.position);
		srcDstDist = Vector3.Distance (prefabs.source.position, prefabs.destination.position);
		
		if (aud.pitch < 3.0f) {
			aud.pitch += Time.deltaTime;
		}
		if (aud.pitch < 0.5f)
			return;

		if (branches.Count > 0) {
			Hashtable tmpHT = (Hashtable)branches.Clone ();
			ICollection keys = tmpHT.Keys;
			LineRenderer branch;
			foreach (int key in keys) {
				branch = (LineRenderer)branches [key];
				BranchScript branchScript = branch.GetComponent<BranchScript> ();
				if ((branchScript.timeSpawned + timers.branchLife) < Time.time) {
					branches.Remove (key);
					Destroy (branch.gameObject);
				}
			}
		}

		if (prefabs.target.localPosition != prefabs.destination.localPosition) {
			if (Vector3.Distance (Vector3.zero, deltaV1) * Time.deltaTime * (1.0f / timers.timeToPowerUp) > Vector3.Distance (prefabs.target.position, prefabs.destination.position))
				prefabs.target.position = prefabs.destination.position;
			else
				prefabs.target.Translate (deltaV1 * Time.deltaTime * (1.0f / timers.timeToPowerUp));
		}

		if ((Time.time - lastUpdate) < timers.timeToUpdate)
			return;
		lastUpdate = Time.time;
		AnimateArc ();
		DrawArc ();
		RayCast ();
	}
	
	void DrawArc ()
	{
		numVertices = Mathf.RoundToInt (srcTrgDist / lines.keyVertexDist) * (1 + lines.numInterpoles) + 1;
		deltaV2 = (prefabs.target.localPosition - prefabs.source.localPosition) / numVertices;
		Vector3 currentV = prefabs.source.localPosition;
		Vector3[] keyVertices = new Vector3[numVertices];

		prefabs.lightning.SetVertexCount (numVertices);
		for (int i = 0; i < numVertices; i++) {
			Vector3 tmpV = currentV;
			float chance;
			tmpV.y += (Random.value * 2.0f - 1.0f) * lines.keyVertexRange;
			tmpV.z += (Random.value * 2.0f - 1.0f) * lines.keyVertexRange;
			prefabs.lightning.SetPosition (i, tmpV);
			keyVertices [i] = tmpV;
			if (branches.ContainsKey (i) == false) {
				chance = Random.value;
				if (chance < dynamics.chanceToArc) {
					LineRenderer tmpLR;
					tmpLR = (LineRenderer)GameObject.Instantiate (prefabs.branch, Vector3.zero, Quaternion.identity);
					tmpLR.GetComponent<BranchScript> ().timeSpawned = Time.time;
					tmpLR.transform.parent = prefabs.lightning.transform;
					branches.Add (i, tmpLR);
					tmpLR.transform.position = prefabs.lightning.transform.TransformPoint (tmpV);
					tmpV.x = Random.value - 0.5f;
					tmpV.y = (Random.value - 0.5f) * 2.0f;
					tmpV.z = (Random.value - 0.5f) * 2.0f;
					tmpLR.transform.LookAt (tmpLR.transform.TransformPoint (tmpV));
					// Length of branches: 12.0f
					tmpLR.transform.Find ("stop").localPosition = tmpLR.transform.Find ("start").localPosition + new Vector3 (0.0f, 0.0f, Random.Range (lines.minBranchLength, lines.maxBranchLength));
					// Set this 12.0f to match the above length of branches
					int brNumVertices = Mathf.RoundToInt (Vector3.Distance (tmpLR.transform.Find ("start").position, tmpLR.transform.Find ("stop").position) / lines.keyVertexDist) * (1 + lines.numInterpoles) + 1;
					Vector3 brDeltaV = (tmpLR.transform.Find ("stop").localPosition - tmpLR.transform.Find ("start").localPosition) / brNumVertices;
					Vector3 brCurrentV = tmpLR.transform.Find ("start").localPosition;
					Vector3[] brKeyVertices = new Vector3[brNumVertices];
					tmpLR.SetVertexCount (brNumVertices);
					for (int j = 0; j < brNumVertices; j++) {
						Vector3 brTmpV = brCurrentV;
						brTmpV.x += (Random.value * 2.0f - 1.0f) * lines.keyVertexRange;
						brTmpV.y += (Random.value * 2.0f - 1.0f) * lines.keyVertexRange;
						tmpLR.SetPosition (j, brTmpV);
						brKeyVertices [j] = brTmpV;
						brCurrentV += brDeltaV * (lines.numInterpoles + 1);
						j += lines.numInterpoles;
					}
					tmpLR.SetPosition (0, tmpLR.transform.Find ("start").localPosition);
					tmpLR.SetPosition (brNumVertices - 1, tmpLR.transform.Find ("stop").localPosition);
					for (int j = 0; j < brNumVertices; j++) {
						Vector3 tmp1, tmp2;
						float x;
						if (j % (lines.numInterpoles + 1) == 0)
							continue;
						tmp1 = brKeyVertices [j - 1];
						tmp2 = brKeyVertices [j + lines.numInterpoles];
						x = (Vector3.Distance (tmp1, tmp2) / ((float)(lines.numInterpoles + 1))) / Vector3.Distance (tmp1, tmp2) * Mathf.PI;
						for (int k = 0; k < lines.numInterpoles; k++) {
							Vector3 brTmpV;
							brTmpV.x = tmp1.x + (brDeltaV.x * (1 + k));
							brTmpV.y = tmp1.y + (Mathf.Sin (x - (Mathf.PI / 2.0f)) / 2.0f + 0.5f) * (tmp2.y - tmp1.y);
							brTmpV.z = tmp1.z + (Mathf.Sin (x - (Mathf.PI / 2.0f)) / 2.0f + 0.5f) * (tmp2.z - tmp1.z);
							tmpLR.SetPosition (j + k, brTmpV);
							x += x;
						}
						j += lines.numInterpoles;
					}
				}
			} else {
				LineRenderer branch = (LineRenderer)branches [i];
				// Set this 12.0f to match the above length of branches
				int brNumVertices = Mathf.RoundToInt (Vector3.Distance (branch.transform.Find ("start").position, branch.transform.Find ("stop").position) / lines.keyVertexDist) * (1 + lines.numInterpoles) + 1;
				Vector3 brDeltaV = (branch.transform.Find ("stop").localPosition - branch.transform.Find ("start").localPosition) / brNumVertices;
				Vector3 brCurrentV = branch.transform.Find ("start").localPosition;
				Vector3[] brKeyVertices = new Vector3[brNumVertices];
				branch.SetVertexCount (brNumVertices);
				branch.SetPosition (0, branch.transform.Find ("start").localPosition);
				for (int j = 0; j < brNumVertices; j++) {
					Vector3 brTmpV = brCurrentV;
					brTmpV.x += (Random.value * 2.0f - 1.0f) * lines.keyVertexRange;
					brTmpV.y += (Random.value * 2.0f - 1.0f) * lines.keyVertexRange;
					branch.SetPosition (j, brTmpV);
					brKeyVertices [j] = brTmpV;
					brCurrentV += brDeltaV * (lines.numInterpoles + 1);
					j += lines.numInterpoles;
				}
				branch.SetPosition (0, branch.transform.Find ("start").localPosition);
				branch.SetPosition (brNumVertices - 1, branch.transform.Find ("stop").localPosition);
				for (int j = 0; j < brNumVertices; j++) {
					Vector3 tmp1, tmp2;
					float x;
					if (j % (lines.numInterpoles + 1) == 0)
						continue;
					tmp1 = brKeyVertices [j - 1];
					tmp2 = brKeyVertices [j + lines.numInterpoles];
					x = (Vector3.Distance (tmp1, tmp2) / ((float)(lines.numInterpoles + 1))) / Vector3.Distance (tmp1, tmp2) * Mathf.PI;
					for (int k = 0; k < lines.numInterpoles; k++) {
						Vector3 brTmpV;
						brTmpV.x = tmp1.x + (brDeltaV.x * (1 + k));
						brTmpV.y = tmp1.y + (Mathf.Sin (x - (Mathf.PI / 2.0f)) / 2.0f + 0.5f) * (tmp2.y - tmp1.y);
						brTmpV.z = tmp1.z + (Mathf.Sin (x - (Mathf.PI / 2.0f)) / 2.0f + 0.5f) * (tmp2.z - tmp1.z);
						branch.SetPosition (j + k, brTmpV);
						x += x;
					}
					j += lines.numInterpoles;
				}
			}
			currentV += deltaV2 * (lines.numInterpoles + 1);
			i += lines.numInterpoles;
		}
		prefabs.lightning.SetPosition (0, prefabs.source.localPosition);
		prefabs.lightning.SetPosition (numVertices - 1, prefabs.target.localPosition);
		for (int i = 0; i < numVertices; i++) {
			Vector3 tmp1, tmp2;
			float x;
			if (i % (lines.numInterpoles + 1) == 0)
				continue;
			tmp1 = keyVertices [i - 1];
			tmp2 = keyVertices [i + lines.numInterpoles];
			x = (Vector3.Distance (tmp1, tmp2) / ((float)(lines.numInterpoles + 1))) / Vector3.Distance (tmp1, tmp2) * Mathf.PI;
			for (int j = 0; j < lines.numInterpoles; j++) {
				Vector3 tmpV;
				tmpV.x = tmp1.x + (deltaV2.x * (1 + j));
				tmpV.y = tmp1.y + (Mathf.Sin (x - (Mathf.PI / 2.0f)) / 2.0f + 0.5f) * (tmp2.y - tmp1.y);
				tmpV.z = tmp1.z + (Mathf.Sin (x - (Mathf.PI / 2.0f)) / 2.0f + 0.5f) * (tmp2.z - tmp1.z);
				prefabs.lightning.SetPosition (i + j, tmpV);
				x += x;
			}
			i += lines.numInterpoles;
		}
	}
	
	void AnimateArc ()
	{
		Vector2 offset = prefabs.lightning.GetComponent<Renderer> ().material.mainTextureOffset;
		Vector2 scale = prefabs.lightning.GetComponent<Renderer> ().material.mainTextureScale;
		
		offset.x += Time.deltaTime * tex.animateSpeed;
		offset.y = tex.offsetY;
		scale.x = srcTrgDist / srcDstDist * tex.scaleX;
		scale.y = tex.scaleY;
		
		prefabs.lightning.GetComponent<Renderer> ().material.mainTextureOffset = offset;
		prefabs.lightning.GetComponent<Renderer> ().material.mainTextureScale = scale;
	}

	void RayCast ()
	{
		RaycastHit[] hits;
		hits = Physics.RaycastAll (prefabs.source.position, prefabs.target.position - prefabs.source.position, Vector3.Distance (prefabs.source.position, prefabs.target.position));
		foreach (RaycastHit hit in hits) {
			Instantiate (prefabs.sparks, hit.point, Quaternion.identity);
		}
		if (branches.Count > 0) {
			Hashtable tmpHT = (Hashtable)branches.Clone ();
			ICollection keys = tmpHT.Keys;
			LineRenderer branch;
			foreach (int key in keys) {
				branch = (LineRenderer)branches [key];
				hits = Physics.RaycastAll (branch.transform.Find ("start").position, branch.transform.Find ("stop").position - branch.transform.Find ("start").position, Vector3.Distance (branch.transform.Find ("start").position, branch.transform.Find ("stop").position));
				foreach (RaycastHit hit in hits) {
					Instantiate (prefabs.sparks, hit.point, Quaternion.identity);
				}
			}
		}
	}
}
