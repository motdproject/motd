using UnityEngine;
using System.Collections;

public class ArcEnd : MonoBehaviour {
	Vector3 s;
	public Light l;

	// Use this for initialization
	void Start () {
	s = transform.localScale;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 relativePos = Camera.main.transform.position - transform.position;
		Quaternion rot = Quaternion.LookRotation(Vector3.up, relativePos);
		transform.rotation = rot;
		transform.Rotate(Vector3.up, Random.value * 360.0f);
		if (Random.value > 0.8f) {
			transform.localScale = s * 1.5f;
			if (l != null) {
				l.range = 100.0f;
				l.intensity = 1.5f;
			}
		}
		else {
			transform.localScale = s;
			if (l != null) {
				l.range = 50.0f;
				l.intensity = 1.0f;
			}
		}
	}
}
