﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


// TODO: real wave management
public class EnemySpawner : Photon.MonoBehaviour
{
	public List<Wave> waves;

	// The path to the target almost never changes, so keep it centralized
	public GameObject target;
	public List<Vector3> pathToTarget;
	public bool drawPathInEditor;

	private float _nextSpawnTime;
	private int _currentWave, _currentEnemyInWave;

	void Start ()
	{
		target = GameObject.FindWithTag (TagNames.PLAYER_BASE);

		if (PhotonNetwork.isMasterClient) {
			gameObject.GetComponent<Pathfinding> ().FindPath (transform.position, target.transform.position);
			StartCoroutine (FindPath ());
		}
	}

	void OnEnable ()
	{
		Messenger.AddListener (EventNames.MAP_CHANGED, OnMapChanged);
	}
	
	void OnDisable ()
	{
		Messenger.RemoveListener (EventNames.MAP_CHANGED, OnMapChanged);
	}

	IEnumerator FindPath ()
	{
		pathToTarget = gameObject.GetComponent<Pathfinding> ().Path;
		if (pathToTarget.Count == 0) {
			yield return new WaitForSeconds (0.1f);
			StartCoroutine (FindPath ());
		} else if (drawPathInEditor) {
			for (int i = 0; i < pathToTarget.Count - 1; i++) {
				//Debug.Log ("Drawing line between " +  pathToTarget[i] + " and " + pathToTarget[i +1]);
				Debug.DrawLine (pathToTarget [i], pathToTarget [i + 1], Color.blue, 1000, true);
			}
		}
	}

	void OnMapChanged ()
	{
		StartCoroutine (FindPath ());
	}

	void Update ()
	{
		if (PhotonNetwork.isMasterClient) {
			Spawn ();
		} 
	}

	void Spawn ()
	{
		if (_currentWave >= waves.Count)
			return;

		Wave currentWave = waves [_currentWave];

		if (Time.time > _nextSpawnTime) {
			if (_currentEnemyInWave++ < currentWave.numberOfEnemies) {
				SpawnEnemy (currentWave);
				_nextSpawnTime = Time.time + currentWave.timeBetweenEachSpawn;
			} else {
				currentWave.finished = true;
				
				// notify the main orchestrator that a wave has finished
				Messenger.Broadcast (EventNames.WAVE_FINISHED);	

				// Prepare the next wave
				_currentEnemyInWave = 0;
				_currentWave++;
				if (_currentWave < waves.Count) {
					Debug.Log ("Setting next index to " + _currentWave + " and wave count is " + waves.Count);
					_nextSpawnTime = waves [_currentWave].startTime;
				}
			}
		}
	}

	void SpawnEnemy (Wave currentWave)
	{
		RaycastHit hit;
		if (!Physics.Raycast (transform.position, Vector3.down, out hit, Mathf.Infinity)) {
			return;
		}
		//Vector3 pos = this.transform.position + Vector3.up;
		//pos.x += (0.5f - Random.value) * 5;
		//pos.y += (0.5f - Random.value) * 5;
		GameObject enemyPrefab = currentWave.NextEnemy (_currentEnemyInWave);
		object[] data = new object[2];
		data [0] = photonView.viewID;
		data [1] = currentWave.difficultyMultiplier;
		PhotonNetwork.InstantiateSceneObject (enemyPrefab.name, hit.point, Quaternion.identity, 0, data);	      
	}

	public void AddWave(Wave wave) {
		Debug.Log ("Adding wave " + wave);
		waves.Add (wave);

		if (waves.Count == 1) {
			_nextSpawnTime = Time.time + waves [0].startTime;
		}	
	}
}
