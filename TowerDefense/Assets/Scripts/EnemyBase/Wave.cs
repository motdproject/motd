﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Wave
{
	public int numberOfEnemies;
	public float timeBetweenEachSpawn;
	//public float timeAfterPreviousWave;
	public List<GameObject> enemyTypes = new List<GameObject> ();
	public float startTime;
	public float difficultyMultiplier;
	public bool finished;

	public GameObject NextEnemy (int currentEnemyInWave)
	{
		return enemyTypes [currentEnemyInWave % enemyTypes.Count];
	}

	public Wave Clone() {
		Wave clone = new Wave ();
		clone.numberOfEnemies = this.numberOfEnemies;
		clone.timeBetweenEachSpawn = this.timeBetweenEachSpawn;
		clone.enemyTypes = this.enemyTypes;
		clone.startTime = this.startTime;
		clone.difficultyMultiplier = this.difficultyMultiplier;
		clone.finished = this.finished;
		return clone;
	}

}