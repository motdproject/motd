﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class WaveRules {

	public GameObject enemyType;
	public int initialWaveSize;
	public float delayBeforeFirstWave, delayBetweenWaves, delayBetweenEnemies;
	public float numberOfWavesBeforeDoublingDifficulty;
}
