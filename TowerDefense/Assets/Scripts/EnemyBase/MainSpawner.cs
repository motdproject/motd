﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainSpawner : MonoBehaviour {
	
	public WaveRules rules;

	[SerializeField]
	private List<Wave> waves = new List<Wave>();
	[SerializeField]
	private List<EnemySpawner> spawners = new List<EnemySpawner>();
	[SerializeField]
	private Dictionary<Wave, List<Wave>> waveList = new Dictionary<Wave, List<Wave>>();

	private int _currentWave;
	private float _latestStartTime, _latestWaveDuration;
	
	void OnEnable ()
	{
		Messenger<EnemySpawner>.AddListener (EventNames.SPAWNER_CREATED, OnSpawnerCreated);
		Messenger.AddListener (EventNames.WAVE_FINISHED, OnWaveFinished);
		Messenger.AddListener (EventNames.MAP_READY, OnMapReady);
	}
	
	void OnDisable ()
	{
		Messenger<EnemySpawner>.RemoveListener (EventNames.SPAWNER_CREATED, OnSpawnerCreated);
		Messenger.RemoveListener (EventNames.WAVE_FINISHED, OnWaveFinished);
		Messenger.RemoveListener (EventNames.MAP_READY, OnMapReady);
	}

	void OnSpawnerCreated(EnemySpawner spawner) {
		Debug.Log ("Spawner created");	
		spawners.Add (spawner);
	}

	void OnMapReady ()
	{
		if (PhotonNetwork.isMasterClient) {
			_latestStartTime = Time.time;
			for (int i = 0; i < 4; i++) {
			 	Wave wave = BuildWave(rules, i);
				waves.Add(wave);
			}
			RegisterWaves ();
		} 
	}

	Wave BuildWave(WaveRules rules, int waveIndex) {
		 // For now, only one allowed enemy type
		GameObject enemyType = rules.enemyType;

		// The size of the initial waves
		int initialWaveSize = rules.initialWaveSize;

		// The delay before the first wave
		float delayBeforeFirstWave = rules.delayBeforeFirstWave;

		// The delay between waves
		float delayBetweenWaves = rules.delayBetweenWaves;

		// The delay between enemies of the wave
		float delayBetweenEnemies = rules.delayBetweenEnemies;

		// How fast does the difficulty scale
		float numberOfWavesBeforeDoublingDifficulty = rules.numberOfWavesBeforeDoublingDifficulty;

		Wave wave = new Wave ();
		wave.startTime = _latestStartTime + _latestWaveDuration + (waveIndex == 0 ? delayBeforeFirstWave : delayBetweenWaves);
		wave.enemyTypes.Add(enemyType);
		wave.numberOfEnemies = initialWaveSize;
		wave.timeBetweenEachSpawn = delayBetweenEnemies;

		wave.difficultyMultiplier = 1 + waveIndex / numberOfWavesBeforeDoublingDifficulty;

		_latestStartTime = wave.startTime;
		_latestWaveDuration = initialWaveSize * delayBetweenEnemies;

		return wave;
	}

	void RegisterWaves ()
	{
		foreach (Wave wave in waves) {
			RegisterWave(wave);
		}
	}

	void RegisterWave(Wave wave) {
		Debug.Log ("Registering wave");
		List<Wave> subWaves = new List<Wave>();
		List<EnemySpawner> spawnersForWave = GetSpawners (wave);
		if (spawnersForWave.Count == 0) {
			spawnersForWave.Add (spawners [UnityEngine.Random.Range (0, spawners.Count - 1)]);
		}

		foreach (EnemySpawner spawner in spawnersForWave) {
			Debug.Log ("Spawner is " + spawner);
			Wave subWave = GetWaveForSpawner(wave, spawner, spawnersForWave);
			spawner.AddWave(subWave);
			subWaves.Add(subWave);
		}

		waveList.Add(wave, subWaves);
	}

	List<EnemySpawner> GetSpawners(Wave wave) {
		Debug.Log ("Getting spawners");
		Debug.Log ("Spawner list is " + spawners.Count);
		// For now, simple random
		List<EnemySpawner> spawnersForWave = new List<EnemySpawner> ();
		foreach (EnemySpawner spawner in spawners) {
			float rnd = UnityEngine.Random.Range (0f, 1f);
			//Debug.Log ("Random is " + rnd);
			if (rnd < 0.5f) {
				spawnersForWave.Add(spawner);
			}
		}

		return spawnersForWave;
	}


	Wave GetWaveForSpawner(Wave wave, EnemySpawner spawner, List<EnemySpawner> spawnersForWave) {
		// For now simply feed a copy of the initial wave to each of the spawners, and scale the number of enemies appropriately
		Wave sub = wave.Clone ();
		sub.numberOfEnemies = wave.numberOfEnemies / spawnersForWave.Count;
		return sub;
	}

	void OnWaveFinished() {
		Debug.Log ("On wave " + _currentWave + " finished");
		Wave currentWave = waves[_currentWave];
		List<Wave> currentSubWaves = new List<Wave> ();
		waveList.TryGetValue (currentWave, out currentSubWaves);
		Debug.Log ("There are " + currentSubWaves.Count + " subwaves");

		bool waveFinished = true;
		Debug.Log ("Is there a subwave not finished?");
		foreach (Wave sub in currentSubWaves) {
			if (sub.finished == false) {
				Debug.Log ("Yes, we're not finished yet. Start time of sub is " + sub.startTime);
				waveFinished = false;
			}
		}

		if (waveFinished) {
			_currentWave++;
			Wave newWave = BuildWave(rules, waves.Count);
			waves.Add(newWave);
			RegisterWave(newWave);
		}
	}
}
