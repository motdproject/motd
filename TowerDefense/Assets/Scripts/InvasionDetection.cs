﻿using UnityEngine;
using System.Collections;

public class InvasionDetection : MonoBehaviour
{
	
	private PhotonView _photonView;
	
	void Start ()
	{
		_photonView = GetComponent<PhotonView> ();
	}

	void OnTriggerStay (Collider other)
	{
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.TOWER && other.GetComponent<TowerVictory> () != null) {
			//Debug.Log ("Winning...");
			_photonView.RPC ("BaseDestroyed", PhotonTargets.All);
		}
	}

	[RPC]
	void BaseDestroyed ()
	{
		Application.LoadLevel ("EndGame");
	}
}
