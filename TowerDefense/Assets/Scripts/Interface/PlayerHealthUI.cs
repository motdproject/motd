﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerHealthUI : MonoBehaviour
{
	public Text displayPrefab;
	private Dictionary<int, Text> playerDisplays = new Dictionary<int, Text> ();

	void OnEnable ()
	{
		//Messenger<int, float, float>.AddListener (EventNames.HEALTH_CHANGED, OnPlayerHealthChanged);
		Messenger<int>.AddListener (EventNames.PLAYER_SPAWNED, OnPlayerSpawned);
	}
	
	void OnDisable ()
	{
		//Messenger<int, float, float>.RemoveListener (EventNames.HEALTH_CHANGED, OnPlayerHealthChanged);
		Messenger<int>.RemoveListener (EventNames.PLAYER_SPAWNED, OnPlayerSpawned);
	}

	void OnPlayerSpawned (int playerId)
	{
		Text text = GameObject.Instantiate (displayPrefab);
		text.gameObject.transform.SetParent (transform.gameObject.transform);
		playerDisplays.Add (playerId, text);
	}

	// Quick and dirty(?)
	void Update ()
	{
		foreach (int playerId in playerDisplays.Keys) {
			GameObject player = PhotonView.Find (playerId).gameObject;
			Text text = playerDisplays [playerId];
			OwnerIndicator indicator = player.GetComponentInChildren<OwnerIndicator> ();
			PlayerHealth health = player.GetComponent<PlayerHealth> ();
			//Debug.Log (indicator);
			//Debug.Log (health);
			if (health.currentHp <= 0) {
				text.text = "Player " + (indicator.PlayerIndex + 1) + ": Dead";
			} else {
				text.text = "Player " + (indicator.PlayerIndex + 1) + ": " + health.currentHp + " / " + health.maxHp;
			}
			text.color = indicator.playerColors [indicator.PlayerIndex];
		}
	}
}
