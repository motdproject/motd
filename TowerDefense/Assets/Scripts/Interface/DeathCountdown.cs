﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeathCountdown : MonoBehaviour
{
	private Text _text;

	private float _endTime;

	void OnEnable ()
	{
		Messenger<float>.AddListener (EventNames.SHOW_COUNTDOWN, ShowCountDown);
	}
	
	void OnDisable ()
	{
		Messenger<float>.RemoveListener (EventNames.SHOW_COUNTDOWN, ShowCountDown);
	}

	void Start ()
	{
		_text = GetComponent<Text> ();
		_text.enabled = false;
	}

	void ShowCountDown (float duration)
	{
		_text.enabled = true;
		_endTime = Time.time + duration;
		StartCoroutine (CountDown ());
	}

	IEnumerator CountDown ()
	{
		float timeLeft = _endTime - Time.time;
		if (timeLeft > 0) {
			_text.text = "" + (int)timeLeft;
			yield return new WaitForSeconds (0.1f);
			StartCoroutine (CountDown ());
		} else {
			_text.enabled = false;
		}
	}
}
