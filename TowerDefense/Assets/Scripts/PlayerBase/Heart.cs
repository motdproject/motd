﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Heart : Photon.MonoBehaviour
{
	public Text hpText;

	public int hp = 30;
	
	private PhotonView _photonView;
	
	void Start ()
	{
		hpText = GameObject.Find ("Hp label").GetComponent<Text> ();
		_photonView = GetComponent<PhotonView> ();
		UpdateText ();
	}

	void Update ()
	{
		if (hp <= 0) {
			Application.LoadLevel ("GameOver");
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.ENEMY) {
			TakeDamage (1);
			DestroyEnemy (other.gameObject);
		}
	}

	void TakeDamage (int amount)
	{
		hp -= amount;
		_photonView.RPC ("SetHp", PhotonTargets.All, hp);
	}
	
	[RPC]
	void SetHp (int total)
	{
		hp = total;
		UpdateText ();
	}

	void UpdateText ()
	{
		hpText.text = "Heart's health: " + hp.ToString ();

	}

	void DestroyEnemy (GameObject enemy)
	{
		PhotonNetwork.Destroy (enemy);
	}
}
