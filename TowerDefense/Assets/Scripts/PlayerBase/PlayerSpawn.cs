﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerSpawn : Photon.MonoBehaviour
{
	public GameObject playerPrefab;

	public Dictionary<int, int> players = new Dictionary<int, int> ();

	void OnEnable ()
	{
		Messenger.AddListener (EventNames.MAP_READY, OnMapReady);
	}
	
	void OnDisable ()
	{
		Messenger.RemoveListener (EventNames.MAP_READY, OnMapReady);
	}

	// Use this for initialization
	public void Spawn ()
	{
		RaycastHit hit;
		if (!Physics.Raycast (transform.position, Vector3.down, out hit, Mathf.Infinity)) {
			return;
		}

		//Debug.Log ("Spawning player");
		GameObject player = (GameObject)PhotonNetwork.Instantiate (playerPrefab.name, hit.point, Quaternion.identity, 0);
		int playerId = player.GetPhotonView ().viewID;
		//Debug.Log (playerId);
		//player.GetComponentInChildren<OwnerIndicator> ().PlayerIndex = players.Count;
		photonView.RPC ("NewPlayer", PhotonTargets.MasterClient, playerId);
		Camera.main.GetComponent<CameraController> ().Player = player;
	}

	[RPC]
	void NewPlayer (int playerId)
	{
		int index = players.Count;
		//	Debug.Log ("Setting player indicator to " + index);
		players.Add (playerId, index);
		//Debug.Log (players.ToStringFull ());
		photonView.RPC ("NewPlayerId", PhotonTargets.AllBuffered, playerId, index);
	}

	[RPC]
	void NewPlayerId (int playerId, int index)
	{
		//	Debug.Log ("Allocating index " + index + " to player " + playerId);
		PhotonView.Find (playerId).RPC ("AllocatePlayerIndex", PhotonTargets.AllBuffered, index);
		Messenger<int>.Broadcast (EventNames.PLAYER_SPAWNED, playerId);
	}

	void OnMapReady ()
	{
		if (PhotonNetwork.isMasterClient) {
			//Debug.Log ("Map ready");
			Spawn ();
		}
	}
}
