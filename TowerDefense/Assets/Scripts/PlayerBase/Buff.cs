﻿using UnityEngine;
using System.Collections;

public class Buff : Photon.MonoBehaviour
{

	public enum Stat
	{
		HP,
		DAMAGE
	}

	public Stat stat;
	public float multiplier;

	void Start ()
	{
		transform.parent.GetComponentInChildren<AreaOfEffect> ().Size = GetComponent<SphereCollider> ().radius;
	}

	void OnTriggerEnter (Collider other)
	{
		// Avoid triggering the call once per collider, and limit it to once per character
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.PLAYER && other is CharacterController) {
			//Debug.Log ("Buffing player " + other.gameObject.GetPhotonView ().viewID);
			//Debug.Log ("Buffing triggered by " + other);
			BuffPlayer (other.gameObject);
		}
	}
	
	void OnTriggerStay (Collider other)
	{
		// Too costly in terms of RPC costs
		/*if (PhotonNetwork.isMasterClient && other.tag == "Player") {
			BuffPlayer (other.gameObject);
		}*/
	}
	
	void OnTriggerExit (Collider other)
	{
		if (PhotonNetwork.isMasterClient && other.tag == TagNames.PLAYER && other is CharacterController) {
			//Debug.Log ("Debuffing player " + other.gameObject.GetPhotonView ().viewID);
			//Debug.Log ("Debuffing triggered by " + other);
			DebuffPlayer (other.gameObject);
		}
	}
	
	void BuffPlayer (GameObject player)
	{
		player.GetPhotonView ().RPC ("Buff", PhotonTargets.AllBufferedViaServer, new object[] {
			photonView.viewID,
			player.GetPhotonView ().viewID,
			(byte)stat,
			multiplier
		});
	}
	
	void DebuffPlayer (GameObject player)
	{
		player.GetPhotonView ().RPC ("Debuff", PhotonTargets.AllBufferedViaServer, new object[] {
			photonView.viewID,
			player.GetPhotonView ().viewID, 
			(byte)stat});
	}
}
