﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Gold : MonoBehaviour
{
	//public Text goldText;

	public float totalGold = 0;
	public float timeBeforeAutoGoldIncrement = 1f;
	public int goldAutoIncrementAmount = 1;
	
	private PhotonView _photonView;
	private float _lastGoldAutoIncrementTime;

	void OnEnable ()
	{
		Messenger<GameObject>.AddListener (EventNames.ENEMY_KILLED, OnEnemyKilled);
	}
	
	void OnDisable ()
	{
		Messenger<GameObject>.RemoveListener (EventNames.ENEMY_KILLED, OnEnemyKilled);
	}

	void Start ()
	{
		_photonView = GetComponent<PhotonView> ();
		//goldText = GameObject.Find ("Total gold label").GetComponent<Text> ();


		_lastGoldAutoIncrementTime = Time.time;
		UpdateText ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		// Only the master handles gold updates
		if (PhotonNetwork.isMasterClient) {
			// Always increment gold
			if (Time.time > _lastGoldAutoIncrementTime + timeBeforeAutoGoldIncrement) {
				IncrementGold (goldAutoIncrementAmount);
				_lastGoldAutoIncrementTime = Time.time;
			}
		}
	}

	void OnEnemyKilled (GameObject enemy)
	{
		float goldIncrement = enemy.GetComponent<Enemy> ().goldAmount;
		IncrementGold (goldIncrement);
	}

	void IncrementGold (float amount)
	{
		totalGold += amount;
		_photonView.RPC ("SetTotalGold", PhotonTargets.All, totalGold);
	}

	public bool Pay (float amount)
	{
		if (!CanPay (amount))
			return false;

		IncrementGold (-amount);
		return true;
	}

	[RPC]
	void SetTotalGold (float total)
	{
		totalGold = total;
		UpdateText ();
	}

	public bool CanPay (float amount)
	{
		return amount <= totalGold;
	}

	void UpdateText ()
	{
		GameObject.Find ("Total gold label").GetComponent<Text> ().text = "Total Gold: " + ((int)totalGold).ToString ();
	}
}
