﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour
{
	public int numberGridCells;
	public bool drawGridInEditor;
	public GridNode[,] grid;

	private MapGenerator _generator;
	private Vector2 _mapStart, _mapEnd;
	private float _gridCellSize;

	void OnEnable ()
	{
		Messenger.AddListener (EventNames.MAP_GENERATED, OnMapGenerated);
	}
	
	void OnDisable ()
	{
		Messenger.RemoveListener (EventNames.MAP_GENERATED, OnMapGenerated);
	}

	void Start ()
	{
		_generator = GetComponent<MapGenerator> ();
	}
	
	void OnMapGenerated ()
	{
		// TODO: network test, don't see how non-master clients can get the message properly for now
		GameObject[,] map = _generator.goMap;
		GameObject bottomLeft = map [0, 0];
		GameObject topRight = map [map.GetLength (0) - 1, map.GetLength (1) - 1];
		
		// Assume all tiles are square
		MeshRenderer renderer = bottomLeft.GetComponent<MeshRenderer> ();
		float tileSize = renderer.bounds.size.x;
		_gridCellSize = tileSize / numberGridCells;
		//Debug.Log ("Grid cell size is " + _gridCellSize);
		
		_mapStart = new Vector2 (bottomLeft.transform.position.x - tileSize / 2, bottomLeft.transform.position.z - tileSize / 2);
		_mapEnd = new Vector2 (topRight.transform.position.x + tileSize / 2, topRight.transform.position.z + tileSize / 2);

		// And build the grid
		CreateGrid ();
	}

	void CreateGrid ()
	{
		//Debug.Log ("Creating grid)");
		//Find positions for start and end of map
		int startX = (int)_mapStart.x;
		int startZ = (int)_mapStart.y;
		int endX = (int)_mapEnd.x;
		int endZ = (int)_mapEnd.y;
		
		// Width and height of the map
		int width = (int)(endX - startX);
		int height = (int)(endZ - startZ);

		// Number of grid cells in both directions
		int numberOfXCells = (int)(width / _gridCellSize);
		int numberOfZCells = (int)(height / _gridCellSize);

		//Debug.Log (numberOfXCells);
		//Debug.Log (numberOfZCells);
		grid = new GridNode[numberOfXCells, numberOfZCells];

		// Fil up the map
		for (int i = 0; i < numberOfZCells; i++) {
			for (int j = 0; j < numberOfXCells; j++) {
				float x = startX + (j * _gridCellSize);
				float z = startZ + (i * _gridCellSize);

				GridNode node = new GridNode (j, i, x, 1, z);
				//Debug.Log (j + " " + i);
				grid [j, i] = node;

			}
		}
		//DrawGrid ();
	}

	void Update ()
	{
		DrawGrid ();
	}

	void DrawGrid ()
	{
		if (drawGridInEditor && grid != null) {
			//Debug.Log ("Drawing grid");
			for (int i = 0; i < grid.GetLength(1) - 1; i++) {
				for (int j = 0; j < grid.GetLength(0) - 1; j++) {
					GridNode node = grid [j, i];
					Vector3 nodePosition = node.position;

					// Draw a line up
					GridNode nodeUp = grid [j, i + 1];
					Vector3 endUpPosition = nodeUp.position;
					Debug.DrawLine (nodePosition, endUpPosition, Color.green);
					/*if (debug.Contains (nodeUp) && debug.Contains (node)) {
						Debug.DrawLine (nodePosition, endUpPosition, Color.red);
					}*/

					// and a line right
					GridNode nodeRight = grid [j + 1, i];
					Vector3 endRightPosition = nodeRight.position;
					Debug.DrawLine (nodePosition, endRightPosition, Color.green);
					/*if (debug.Contains (nodeRight) && debug.Contains (node)) {
						Debug.DrawLine (nodePosition, endRightPosition, Color.red);
					}*/
				}
			}
		}
	}

	public Vector3 GetNearestGridPoint (Vector3 other)
	{
		for (int i = 0; i < grid.GetLength(1); i++) {
			for (int j = 0; j < grid.GetLength(0); j++) {
				GridNode node = grid [j, i];
				if (Mathf.Abs (node.position.x - other.x) <= _gridCellSize / 2 && Mathf.Abs (node.position.z - other.z) <= _gridCellSize / 2) {
					//Debug.Log ("Nearest node position from " + other + " is " + node.position);
					return node.position;
				}
			}
		}
		return Vector3.zero;
	}

	//public List<GridNode> debug = new List<GridNode> ();
	//public Vector3 originDebug;

	public List<Vector3> GetGridPoints (Vector3 origin, float sizeX, float sizeZ)
	{
		//Debug.Log ("Getting grid points from " + origin + " going " + sizeX + " right and " + sizeZ + " top");
		//debug = new List<GridNode> ();
		//originDebug = origin;
		List<Vector3> gridPoints = new List<Vector3> ();
		//gridPoints.Add (origin);

		for (int i = 0; i < grid.GetLength(1); i++) {
			for (int j = 0; j < grid.GetLength(0); j++) {
				GridNode node = grid [j, i];
				if (node.position.x >= origin.x && node.position.x <= origin.x + sizeX + _gridCellSize && 
					node.position.z >= origin.z && node.position.z <= origin.z + sizeZ + _gridCellSize) {
					//Debug.Log ("Nearest node position from " + other + " is " + node.position);
					gridPoints.Add (node.position);
					//debug.Add (node);
				}
			}
		}

		return gridPoints;
	}
}
