﻿using UnityEngine;
using System.Collections;

public class MapInitializer : MonoBehaviour
{
	public GameObject playerBase;
	public GameObject enemyBase;

	void OnEnable ()
	{
		Messenger.AddListener (EventNames.MAP_GENERATED, OnMapGenerated);
	}
	
	void OnDisable ()
	{
		Messenger.RemoveListener (EventNames.MAP_GENERATED, OnMapGenerated);
	}
	
	void OnMapGenerated ()
	{
		GameObject.Instantiate (Resources.Load ("Pathfinder"), Vector3.zero, Quaternion.identity);
		//PhotonNetwork.InstantiateSceneObject ("Pathfinder", Vector3.zero, Quaternion.identity, 0, null);

		Messenger.Broadcast (EventNames.MAP_READY);
	}
}
