﻿using UnityEngine;
using System.Collections.Generic;

public class MapTools2
{

	public static Tile[][] generateMap (int pathNumber, int pathLength)	{
		int size = 2 * pathLength + 1;
		Tile[][] tempMap = new Tile[size][];
		for (int i=0; i<size; i++) {
			tempMap[i]= new Tile[size];
		}

		MinMax minMax = new MinMax ();
		minMax.maxX = pathLength;
		minMax.minX = pathLength;
		minMax.maxY = pathLength;
		minMax.minY = pathLength;

		Tile center = new Tile ();
		center.type = TileType.PLAYER_BASE;
		tempMap [pathLength] [pathLength] = center;


		List<MapDirection> dirs = getDirections(pathNumber);

		foreach (MapDirection dir in dirs) {
			center.directions.Add(dir);
			buildPath (dir, pathLength, tempMap, minMax);
		}

		Tile[][] map = cropMap(tempMap, minMax);
		addSidewalks(map);
		addDecor(map);
		return map;
	}

	private static List<MapDirection> getDirections(int nb) {
		
		List<MapDirection> ret = new List<MapDirection> ();
		switch (nb) {
		case 1:
			ret.Add (MapDirection.EAST);
			break;
		case 2:
			ret.Add (MapDirection.EAST);
			ret.Add (MapDirection.WEST);
			break;
		case 3:
			ret.Add (MapDirection.NORTH);
			ret.Add (MapDirection.EAST);
			ret.Add (MapDirection.SOUTH);
			break;
		case 4:
			ret.Add (MapDirection.NORTH);
			ret.Add (MapDirection.EAST);
			ret.Add (MapDirection.SOUTH);
			ret.Add (MapDirection.WEST);
			break;
		default:
			break;
		}
		return ret;
	}

	private static void buildPath (MapDirection dir, int pathLength, Tile[][] map, MinMax minMax)
	{

		Coord previousTileV = new Coord ();
		previousTileV.x = pathLength;
		previousTileV.y = pathLength;
		MapDirection nextDir = dir;
		
		int nb = pathLength;
		
		while (nb >= 0) {
			Coord newTileV = computeNewTile (nextDir, previousTileV);
			updateMinMax(newTileV, minMax);

			Tile newTile = map[newTileV.x][newTileV.y];
			if (newTile == null) {
				newTile = new Tile ();
				map [(int)newTileV.x] [(int)newTileV.y] = newTile;
			}

			MapDirection from = DirectionTools.getOpposite(nextDir);
			if (!newTile.directions.Contains(from)) {
				newTile.directions.Add(from);
			}

			if (nb > 0) {
			
				nextDir = getPossibleDirection (from, newTileV, dir, nb, map);
				if (!newTile.directions.Contains(nextDir)) {
					newTile.directions.Add(nextDir);
				}

				previousTileV = newTileV;
				newTile.type = TileType.PATH;
			} else {
				newTile.type = TileType.SPAWN;
			}

			nb--;
		}
	}

	private static Coord computeNewTile(MapDirection dir, Coord previous) {
		Coord newV = new Coord();
		switch (dir) {
		case MapDirection.NORTH:
			newV.x = previous.x;
			newV.y = previous.y + 1;
			break;
		case MapDirection.SOUTH:
			newV.x = previous.x;
			newV.y = previous.y - 1;
			break;
		case MapDirection.EAST:
			newV.x = previous.x + 1;
			newV.y = previous.y;
			break;
		case MapDirection.WEST:
			newV.x = previous.x - 1;
			newV.y = previous.y;
			break;
		default:
			// should not happen
			newV.x = previous.x;
			newV.y = previous.y;
			break;
		}		
		return newV;
	}

	private static MapDirection getPossibleDirection (MapDirection from, Coord tileV, MapDirection initialDir, int n, Tile[][] map)
	{
		List<MapDirection> possDir = new List<MapDirection> {MapDirection.EAST, MapDirection.NORTH, MapDirection.SOUTH, MapDirection.WEST};
		removeIncompatibleDirections (possDir, from, tileV, initialDir, n, map);
		if (possDir.Count == 0) {
			// TODO throw error
			return MapDirection.NOWHERE;
		} else {
			return (MapDirection)possDir [DirectionTools.getRandom (0, possDir.Count - 1)];
		}

	}

	private static void removeIncompatibleDirections (List<MapDirection> possDir, MapDirection from, Coord tileV, MapDirection initialDir, int nb, Tile[][] map)
	{
		// TODO improve
		possDir.Remove (from);
		possDir.Remove (DirectionTools.getOpposite (initialDir));
		if (isSpawn(map[tileV.x][tileV.y+1])) {
			possDir.Remove(MapDirection.NORTH);
		}
		if (isSpawn(map[tileV.x+1][tileV.y])) {
			possDir.Remove(MapDirection.EAST);
		}
		if (isSpawn(map[tileV.x][tileV.y-1])) {
			possDir.Remove(MapDirection.SOUTH);
		}
		if (isSpawn(map[tileV.x-1][tileV.y])) {
			possDir.Remove(MapDirection.WEST);
		}
		if (nb < 4) {
			if (isPath(map[tileV.x][tileV.y+1])) {
				possDir.Remove(MapDirection.NORTH);
			}
			if (isPath(map[tileV.x+1][tileV.y])) {
				possDir.Remove(MapDirection.EAST);
			}
			if (isPath(map[tileV.x][tileV.y-1])) {
				possDir.Remove(MapDirection.SOUTH);
			}
			if (isPath(map[tileV.x-1][tileV.y])) {
				possDir.Remove(MapDirection.WEST);
			}
		}
	}

	private static bool isSpawn(Tile t) {
		return t != null && t.type == TileType.SPAWN;
	}
	
	
	private static bool isPath(Tile t) {
		return t != null && t.type == TileType.PATH;
	}

	/**
	 * Minmax = (x_min, x_max, y_min, y_max)
	 */

	private static void updateMinMax(Coord v, MinMax minMax) {
		//Debug.Log (Time.time + "Avant : " + v + " / " + minMax);
		if (v.x < minMax.minX) {
			minMax.minX = v.x;
		}else if (v.x > minMax.maxX) {
			minMax.maxX = v.x;
		}
		if (v.y < minMax.minY) {
			minMax.minY = v.y;
		} else if (v.y > minMax.maxY) {
			minMax.maxY = v.y;
		}
		//Debug.Log (Time.time + "Après : " + v + " / " + minMax);
	}

	private static Tile[][] cropMap(Tile[][] tempMap, MinMax minMax) {
		int xSize = minMax.maxX - minMax.minX + 5;
		int ySize = minMax.maxY - minMax.minY + 5;

		Tile[][] map = new Tile[xSize][];
		
		for (int i=0; i<xSize; i++) {
			map[i]= new Tile[ySize];
			if (i>1 && i<xSize-2) {
				for (int j=2; j<ySize-2; j++) {
					map[i][j] = tempMap [minMax.minX + i -2][minMax.minY + j -2];
				}
			}
		}


		return map;
	}
	
	private static void addDecor(Tile[][] map) {
		
		// Bords de la map
		for (int i=0; i<map.Length; i++) {
			for (int j=0; j<map[0].Length; j++) {
				Tile tile = map[i][j];
				if(tile == null) {
					Tile decor = new Tile ();
					decor.type = TileType.DECOR;
					map[i][j] = decor;
				}
			}
		}
	}

	private static void addSidewalks(Tile[][] map) {

		// Bords de la map
		for (int i=0; i<map.Length; i++) {
			Tile bTile = new Tile();
			bTile.type = TileType.BORDER;
			bTile.directions.Add(MapDirection.WEST);
			map[i][0] = bTile;
			
			Tile bTile2 = new Tile();
			bTile2.type = TileType.BORDER;
			bTile2.directions.Add(MapDirection.EAST);
			map[i][map[0].Length-1] = bTile2;
		}
		for (int j=0; j<map[0].Length; j++) {
			Tile bTile = new Tile();
			bTile.type = TileType.BORDER;
			bTile.directions.Add(MapDirection.NORTH);
			map[0][j] = bTile;
			
			Tile bTile2 = new Tile();
			bTile2.type = TileType.BORDER;
			bTile2.directions.Add(MapDirection.SOUTH);
			map[map.Length-1][j] = bTile2;
		}

		// mettre les bords de chemin
		for (int i=1; i<map.Length-1; i++) {
			for(int j=1; j<map[0].Length-1; j++) {
				Tile tile = map[i][j];
				if(tile != null && (tile.type == TileType.PATH
				                    || tile.type == TileType.PLAYER_BASE
				                    || tile.type == TileType.SPAWN)) {
					List<MapDirection> dirs = tile.directions;
					if (!dirs.Contains(MapDirection.NORTH)) {
						updateDir(i-1,j+1, MapDirection.SOUTHEAST, map);
						if (map[i][j+1] == null) {
							Tile sTile = new Tile();
							sTile.type = TileType.SIDEWALK;
							sTile.directions.Add(MapDirection.SOUTHEAST);
							sTile.directions.Add(MapDirection.SOUTHWEST);
							map[i][j+1] = sTile;
						} else if (map[i][j+1].type == TileType.SIDEWALK) {
							if (!map[i][j+1].directions.Contains(MapDirection.SOUTHEAST)){
								map[i][j+1].directions.Add(MapDirection.SOUTHEAST);
							}
							if (!map[i][j+1].directions.Contains(MapDirection.SOUTHWEST)){
								map[i][j+1].directions.Add(MapDirection.SOUTHWEST);
							}
						}
						updateDir(i+1,j+1, MapDirection.SOUTHWEST, map);
					}
					if (!dirs.Contains(MapDirection.SOUTH)) {
						updateDir(i-1,j-1, MapDirection.NORTHEAST, map);
						if (map[i][j-1] == null) {
							Tile sTile = new Tile();
							sTile.type = TileType.SIDEWALK;
							sTile.directions.Add(MapDirection.NORTHEAST);
							sTile.directions.Add(MapDirection.NORTHWEST);

							map[i][j-1] = sTile;
						} else if (map[i][j-1].type == TileType.SIDEWALK) {
							if (!map[i][j-1].directions.Contains(MapDirection.NORTHEAST)){
								map[i][j-1].directions.Add(MapDirection.NORTHEAST);
							}
							if (!map[i][j-1].directions.Contains(MapDirection.NORTHWEST)){
								map[i][j-1].directions.Add(MapDirection.NORTHWEST);
							}
						}
						updateDir(i+1,j-1, MapDirection.NORTHWEST, map);
					}
					if (!dirs.Contains(MapDirection.EAST)) {
						updateDir(i+1,j-1, MapDirection.NORTHWEST, map);
						if (map[i+1][j] == null) {
							Tile sTile = new Tile();
							sTile.type = TileType.SIDEWALK;
							sTile.directions.Add(MapDirection.NORTHWEST);
							sTile.directions.Add(MapDirection.SOUTHWEST);
							map[i+1][j] = sTile;
						} else if (map[i+1][j].type == TileType.SIDEWALK) {
							if (!map[i+1][j].directions.Contains(MapDirection.NORTHWEST)){
								map[i+1][j].directions.Add(MapDirection.NORTHWEST);
							}
							if (!map[i+1][j].directions.Contains(MapDirection.SOUTHWEST)){
								map[i+1][j].directions.Add(MapDirection.SOUTHWEST);
							}
						}
						updateDir(i+1,j+1, MapDirection.SOUTHWEST, map);
					}
					if (!dirs.Contains(MapDirection.WEST)) {
						updateDir(i-1,j-1, MapDirection.NORTHEAST, map);
						if (map[i-1][j] == null) {
							Tile sTile = new Tile();
							sTile.type = TileType.SIDEWALK;
							sTile.directions.Add(MapDirection.NORTHEAST);
							sTile.directions.Add(MapDirection.SOUTHEAST);
							map[i-1][j] = sTile;
						} else if (map[i-1][j].type == TileType.SIDEWALK) {
							if (!map[i-1][j].directions.Contains(MapDirection.NORTHEAST)){
								map[i-1][j].directions.Add(MapDirection.NORTHEAST);
							}
							if (!map[i-1][j].directions.Contains(MapDirection.SOUTHEAST)){
								map[i-1][j].directions.Add(MapDirection.SOUTHEAST);
							}
						}
						updateDir(i-1,j+1, MapDirection.SOUTHEAST, map);
					}
				}
			}
		}

	}

	private static void updateDir(int i, int j, MapDirection dir, Tile[][] map) {
		if (map[i][j] == null) {
			Tile sTile = new Tile();
			sTile.type = TileType.SIDEWALK;
			sTile.directions.Add(dir);
			
			map[i][j] = sTile;
		} else if (map[i][j].type == TileType.SIDEWALK 
		           && !map[i][j].directions.Contains(dir)){
			map[i][j].directions.Add(dir);
		}
	}
}
