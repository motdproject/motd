public struct TilePoint
{
	public MapDirection direction;
	public int number;

	public override string ToString ()
	{
		return string.Format ("[{0}, {1}]", direction, number);
	}
}
	