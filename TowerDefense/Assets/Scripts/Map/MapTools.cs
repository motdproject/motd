﻿using UnityEngine;
using System.Collections;

public class MapTools
{
	/*
	public static ArrayList generateMap (int pathNumber, int pathLength)
	{
		ArrayList ret = new ArrayList ();
		switch (pathNumber) {
		case 1:
			ret.Add (buildPath (MapDirection.EAST, 1, pathLength));
			break;
		case 2:
			ret.Add (buildPath (MapDirection.EAST, 2, pathLength));
			ret.Add (buildPath (MapDirection.WEST, 2, pathLength));
			break;
		case 3:
			ret.Add (buildPath (MapDirection.NORTH, 4, pathLength));
			ret.Add (buildPath (MapDirection.EAST, 4, pathLength));
			ret.Add (buildPath (MapDirection.SOUTH, 4, pathLength));
			break;
		default:
			break;
		}
		return ret;
	}

	private static Tile[] buildPath (MapDirection dir, int part, int pathLength)
	{
		Tile[] path = new Tile[pathLength];
		MapDirection[] pathDir = new MapDirection[pathLength];
		//debugLog (path, pathDir);
		int nb = 0;
		pathDir [nb] = dir;
		while (nb < pathLength) {
			addNewTile (path, pathDir, part, nb);
			nb++;
		}


		return path;
	}

	private static void addNewTile (Tile[] path, MapDirection[] pathDir, int part, int nb)
	{
		MapDirection from = DirectionTools.getOpposite (pathDir [nb]);
		MapDirection to = getPossibleDirection (from, pathDir, part);
		int fromNb = 1;
		if (nb > 0) {
			fromNb = path [nb - 1].arrival.number;
		} else {
			fromNb = DirectionTools.getRandom (1, 2);
		}
		path [nb] = createNewTile (from, fromNb, to);

		if (nb < pathDir.Length - 1) {
			pathDir [nb + 1] = to;
		}

	}

	private static MapDirection getPossibleDirection (MapDirection from, MapDirection[] pathDir, int part)
	{
		ArrayList possDir = new ArrayList {MapDirection.EAST, MapDirection.NORTH, MapDirection.SOUTH, MapDirection.WEST};
		possDir.Remove (from);
		removeIncompatibleDirections (possDir, pathDir, part);
		if (pathDir [pathDir.Length - 1] != MapDirection.EMPTY) {
			// dernière tuile
			return MapDirection.NOWHERE;
		} else if (possDir.Count == 0) {
			// TODO throw error
			return MapDirection.NOWHERE;
		} else {
			return (MapDirection)possDir [DirectionTools.getRandom (0, possDir.Count - 1)];
		}

	}

	private static void removeIncompatibleDirections (ArrayList possDir, MapDirection[] pathDir, int part)
	{
		possDir.Remove (DirectionTools.getOpposite (pathDir [0]));
	}

	private static Tile createNewTile (MapDirection from, int fromNb, MapDirection to)
	{
		Tile tile = new Tile ();
		TilePoint start = new TilePoint ();
		start.direction = from;
		start.number = fromNb;
		TilePoint arrival = new TilePoint ();
		arrival.direction = to;
		arrival.number = DirectionTools.getRandom (1, 2);
		tile.start = start;
		tile.arrival = arrival;
		return tile;
	}

	private static void debugLog (Tile[] a, MapDirection[] b)
	{
		for (int i =0; i<a.Length; i++) {
			Debug.Log (i + " : " + a [i]);
		}
		for (int i =0; i<b.Length; i++) {
			Debug.Log (i + " : " + b [i]);
		}
	}

	public static Turning isTurning (Tile tile)
	{
		Turning ret = Turning.STRAIGHT;
		switch (tile.start.direction) {
			
		case MapDirection.NORTH:
			switch (tile.arrival.direction) {
			case MapDirection.NOWHERE:
				ret = Turning.DEADEND;
				break;
			case MapDirection.EAST:
				ret = Turning.LEFT;
				break;
			case MapDirection.WEST:
				ret = Turning.RIGHT;
				break;
			case MapDirection.NORTH:
			case MapDirection.SOUTH:
			default:
				ret = Turning.STRAIGHT;
				break;
			}		
			break;
		case MapDirection.SOUTH:
			switch (tile.arrival.direction) {
			case MapDirection.NOWHERE:
				ret = Turning.DEADEND;
				break;
			case MapDirection.EAST:
				ret = Turning.RIGHT;
				break;
			case MapDirection.WEST:
				ret = Turning.LEFT;
				break;
			case MapDirection.NORTH:
			case MapDirection.SOUTH:
			default:
				ret = Turning.STRAIGHT;
				break;
			}		
			break;
		case MapDirection.EAST:
			switch (tile.arrival.direction) {
			case MapDirection.NOWHERE:
				ret = Turning.DEADEND;	
				break;			
			case MapDirection.NORTH:
				ret = Turning.RIGHT;
				break;
			case MapDirection.SOUTH:
				ret = Turning.LEFT;
				break;
			case MapDirection.EAST:
			case MapDirection.WEST:
				ret = Turning.STRAIGHT;
				break;
			default:
				break;
			}		
			break;
		case MapDirection.WEST:
			switch (tile.arrival.direction) {
			case MapDirection.NOWHERE:
				ret = Turning.DEADEND;
				break;
			case MapDirection.NORTH:
				ret = Turning.LEFT;
				break;
			case MapDirection.SOUTH:
				ret = Turning.RIGHT;
				break;
			case MapDirection.EAST:
			case MapDirection.WEST:
			default:
				ret = Turning.STRAIGHT;
				break;
			}		
			break;
		default:
			break;
		}		
		return ret;
	}
*/
}
