﻿using UnityEngine;
using System.Collections;

public class DirectionTools  {

	public static TilePoint getOpposite(TilePoint point) {
		TilePoint ret = new TilePoint ();
		ret.number = point.number;
		ret.direction = getOpposite(point.direction);
		return ret;
	}
	
	public static MapDirection getOpposite(MapDirection direction) {
		MapDirection dir ;
		switch (direction) {
		case MapDirection.NORTH:
			dir = MapDirection.SOUTH;
			break;
		case MapDirection.SOUTH:
			dir = MapDirection.NORTH;
			break;
		case MapDirection.EAST:
			dir = MapDirection.WEST;
			break;
		case MapDirection.WEST:
			dir = MapDirection.EAST;
			break;
		default:
			dir = MapDirection.NORTH;
			break;
		}		
		return dir;
		
	}


	public static int getRandom(float start, float end) {
		return Mathf.RoundToInt (Random.Range ((float) start, (float)end+0.5f));
	}

}
