﻿using UnityEngine;
using System.Collections.Generic;

//[ExecuteInEditMode]
public class MapGenerator : MonoBehaviour
{
	public int pathSize;
	public int numberOfPaths;
	public GameObject playerBasePrefab;
	public GameObject enemyBasePrefab;

	[System.Serializable]
	public class Tiles
	{
		public GameObject Empty;
		public GameObject Border;
		public GameObject Decor;
		public GameObject End;
		public GameObject Ahead;
		public GameObject Turn;
		public GameObject TShape;
		public GameObject Crossroads;
		public GameObject Sidewalk;
		public GameObject DecorTurn;
		public GameObject DecorSide;
		public GameObject DecorEdge;
		public GameObject Fountain;
	}
	public Tiles tiles;

	public GameObject playerBase;
	public List<GameObject> spawners;
	// The map data structure
	public Tile[][] map;
	// The map 3D objects
	public GameObject[,] goMap;
	
	//private GameObject _mapParent;
	
	void OnEnable ()
	{
		Messenger.AddListener (EventNames.SERVER_CREATED, OnServerCreated);
	}
	
	void OnDisable ()
	{
		Messenger.RemoveListener (EventNames.SERVER_CREATED, OnServerCreated);
	}
	
	void OnServerCreated ()
	{
		Debug.Log ("On server created");
		if (PhotonNetwork.isMasterClient) {
			//_mapParent = GameObject.Find ("Map");
			Tile[][] generatedMap = MapTools2.generateMap (numberOfPaths, pathSize);
			GetComponent<PhotonView> ().RPC ("StructureGenerated", PhotonTargets.AllBuffered, new object[] {generatedMap});
		}
	}

	[RPC]
	void StructureGenerated (Tile[][] pMap)
	{
		map = pMap;
			
		goMap = new GameObject[map.Length, map [0].Length];

		generate (map);
		//MapSize (map.Length, map [0].Length);

		// This notification is important, as it will tell the pathfinder that it can draw the pathfinding map
		Messenger.Broadcast (EventNames.MAP_GENERATED);	
		//<PhotonView> ().RPC ("MapSize", PhotonTargets.OthersBuffered, new object[] {map.Length, map [0].Length});
	}

//TODO: remove
	/*void MapSize (int width, int height)
	{
		//Debug.Log (width + " " + height);
		goMap = new GameObject[width, height];
		//Debug.Log (goMap.GetLength (1));
		int i = 0;
		int j = 0;
		foreach (Transform tile in transform) {
			if (tile.tag != TagNames.TILE && tile.tag != TagNames.DECOR)
				continue;
			//Debug.Log (i + " " + j);
			//Debug.Log (tile.name);
			goMap [i, j] = tile.gameObject;
			j++;
			if (j >= goMap.GetLength (1)) {
				j = 0;
				i++;
			}
		}
		Debug.Log ("Map generated on client");
		Messenger.Broadcast (EventNames.MAP_GENERATED);
	}*/


	void generate (Tile[][] map)
	{
		for (int i=0; i<map.Length; i++) {
			for (int j=0; j<map[0].Length; j++) {
				generateTile (i, j, map [i] [j]);
			}
		}
	}

	void generateTile (int i, int j, Tile tile)
	{
		if (tile != null) {
			Vector3 pos = Vector3.zero;
			pos.x += 8f * i;
			pos.z += 8f * j;

			GameObject go = getCorrespondingObject (pos, tile);
			goMap [i, j] = go;
		}
	}

	private GameObject getCorrespondingObject (Vector3 pos, Tile tile)
	{
		GameObject go;
		GameObject prefab = tiles.Empty;
		//string name = "TestTile";
		Quaternion rot = Quaternion.identity;
		switch (tile.type) {
		case TileType.BORDER:
			prefab = tiles.Border;
			//name = "TestTileBorder";
			break;
		case TileType.DECOR:
			prefab = tiles.Decor;
			//name = "TestTileDecFull";
			break;
		case TileType.PATH:
			switch (tile.directions.Count) {
			case 1:
				prefab = tiles.End;
				//name = "TestTileEnd";
				break;
			case 2:
				if (isStraight (tile)) {
					prefab = tiles.Ahead;
					//name = "TestTileAhead";
					if (tile.directions.Contains (MapDirection.NORTH)) {
						rot = Quaternion.FromToRotation (Vector3.right, Vector3.forward);
					}
				} else {
					prefab = tiles.Turn;
					//name = "TestTileTurn";
					rot = getTurnRotation (tile);
				}
				break;
			case 3:
				prefab = tiles.TShape;
				//name = "TestTileT";
				if (!tile.directions.Contains (MapDirection.WEST)) {
					rot = Quaternion.identity;
				} else if (!tile.directions.Contains (MapDirection.NORTH)) {
					rot = Quaternion.FromToRotation (Vector3.right, Vector3.back);
				} else if (!tile.directions.Contains (MapDirection.EAST)) {
					rot = Quaternion.FromToRotation (Vector3.right, Vector3.left);
				} else if (!tile.directions.Contains (MapDirection.SOUTH)) {
					rot = Quaternion.FromToRotation (Vector3.right, Vector3.forward);
				} 
				break;
			case 4:
				prefab = tiles.Crossroads;
				//name = "TestTile4Ways";
				break;
			default:
				break;
			}
			break;
		case TileType.SPAWN:
			prefab = tiles.End;
			//name = "TestTileEnd";
			switch (tile.directions [0]) {
			case MapDirection.NORTH:
				rot = Quaternion.FromToRotation (Vector3.right, Vector3.back);
				break;
			case MapDirection.EAST:
				rot = Quaternion.FromToRotation (Vector3.right, Vector3.left);
				break;
			case MapDirection.SOUTH:
				rot = Quaternion.FromToRotation (Vector3.right, Vector3.forward);
				break;
			}
			//GameObject spawner = (GameObject)GameObject.Instantiate (enemyBasePrefab, pos + Vector3.up, Quaternion.identity);
			//spawner.transform.parent = gameObject.transform;
			if (PhotonNetwork.isMasterClient) {
				GameObject spawner = PhotonNetwork.InstantiateSceneObject (enemyBasePrefab.name, pos + Vector3.up, Quaternion.identity, 0, null);
				spawner.GetPhotonView ().RPC ("ParentObject", PhotonTargets.AllBuffered, "Map");
				spawners.Add (spawner);
				Messenger<EnemySpawner>.Broadcast (EventNames.SPAWNER_CREATED, spawner.GetComponentInChildren<EnemySpawner> ());	
			}
			break;
		case TileType.PLAYER_BASE:
			//playerBase = (GameObject)GameObject.Instantiate (playerBasePrefab, pos + 2f * Vector3.up, Quaternion.identity);
			//playerBase.transform.parent = gameObject.transform;
			if (PhotonNetwork.isMasterClient) {
				playerBase = PhotonNetwork.InstantiateSceneObject (playerBasePrefab.name, pos + 2f * Vector3.up, Quaternion.identity, 0, null);
				playerBase.GetPhotonView ().RPC ("ParentObject", PhotonTargets.AllBuffered, "Map");
			}
			//playerBase.transform.parent = _mapParent.transform;
			break;
		case TileType.SIDEWALK:
			prefab = tiles.Sidewalk;
			//name = "TestTileWalk";
			switch (tile.directions.Count) {
			case 1:
				prefab = tiles.DecorTurn;
				//name = "TestTileDecTurn";
				switch (tile.directions [0]) {
				case MapDirection.SOUTHWEST:
					rot = Quaternion.identity;
					break;
				case MapDirection.SOUTHEAST:
					rot = Quaternion.FromToRotation (Vector3.right, Vector3.forward);
					break;
				case MapDirection.NORTHWEST:
					rot = Quaternion.FromToRotation (Vector3.right, Vector3.back);
					break;
				case MapDirection.NORTHEAST:
					rot = Quaternion.FromToRotation (Vector3.right, Vector3.left);
					break;
				}
				break;
			case 2:
				if (isConituguous (tile)) {
					prefab = tiles.DecorSide;
					//name = "TestTileDecSide";
					rot = getDecorRotation (tile);
				}
				break;
			case 3:
				prefab = tiles.DecorEdge;
				//name = "TestTileDecEdge";
				if (!tile.directions.Contains (MapDirection.NORTHEAST)) {
					rot = Quaternion.identity;
				} else if (!tile.directions.Contains (MapDirection.NORTHWEST)) {
					rot = Quaternion.FromToRotation (Vector3.right, Vector3.forward);
				} else if (!tile.directions.Contains (MapDirection.SOUTHEAST)) {
					rot = Quaternion.FromToRotation (Vector3.right, Vector3.back);
				} else if (!tile.directions.Contains (MapDirection.SOUTHWEST)) {
					rot = Quaternion.FromToRotation (Vector3.right, Vector3.left);
				} 
				break;
			case 4:
				if (Random.value < 0.3f) {
					Vector3 offset = pos;
					offset.y += 2.75f;
					GameObject fountain = (GameObject)GameObject.Instantiate (tiles.Fountain, offset, Quaternion.identity);
					fountain.transform.parent = gameObject.transform;
					//PhotonNetwork.InstantiateSceneObject ("FountainOne", offset, Quaternion.identity, 0, null);
				}
				break;
			default:
				break;
			}
			break;
		default :
			break;
		}
		
		rot = rot * Quaternion.FromToRotation (Vector3.back, Vector3.down);

		go = (GameObject)GameObject.Instantiate (prefab, pos, rot);
		go.transform.parent = gameObject.transform;

		//go = PhotonNetwork.InstantiateSceneObject (name, pos, rot, 0, null);
		//go.GetPhotonView ().RPC ("ParentObject", PhotonTargets.AllBuffered, "Map");
		//go.transform.parent = _mapParent.transform;

		return go;
	}

	private bool isStraight (Tile tile)
	{
		return (tile.directions.Contains (MapDirection.NORTH)
			&& tile.directions.Contains (MapDirection.SOUTH))
			|| (tile.directions.Contains (MapDirection.EAST) 
			&& tile.directions.Contains (MapDirection.WEST));
	}
	
	private bool isConituguous (Tile tile)
	{
		return (tile.directions.Contains (MapDirection.NORTHEAST)
			&& (tile.directions.Contains (MapDirection.SOUTHEAST)
			|| tile.directions.Contains (MapDirection.NORTHWEST)))
			|| (tile.directions.Contains (MapDirection.SOUTHWEST)
			&& (tile.directions.Contains (MapDirection.SOUTHEAST)
			|| tile.directions.Contains (MapDirection.NORTHWEST)));
	}

	private Quaternion getTurnRotation (Tile tile)
	{
		switch (tile.directions [0]) {
		case MapDirection.NORTH:
			switch (tile.directions [1]) {
			case MapDirection.EAST:
				return Quaternion.identity;
			case MapDirection.WEST:
				return Quaternion.FromToRotation (Vector3.back, Vector3.right);
			}
			break;
		case MapDirection.SOUTH:
			switch (tile.directions [1]) {
			case MapDirection.EAST:
				return Quaternion.FromToRotation (Vector3.left, Vector3.forward);
			case MapDirection.WEST:
				return Quaternion.FromToRotation (Vector3.right, Vector3.left);
			}
			break;
		case MapDirection.EAST:
			switch (tile.directions [1]) {
			case MapDirection.NORTH:
				return Quaternion.identity;
			case MapDirection.SOUTH:
				return Quaternion.FromToRotation (Vector3.left, Vector3.forward);
			}
			break;
		case MapDirection.WEST:
			switch (tile.directions [1]) {
			case MapDirection.NORTH:
				return Quaternion.FromToRotation (Vector3.right, Vector3.forward);
			case MapDirection.SOUTH:
				return Quaternion.FromToRotation (Vector3.right, Vector3.left);
			}
			break;
		}
		return Quaternion.identity;
	}
	
	private Quaternion getDecorRotation (Tile tile)
	{
		switch (tile.directions [0]) {
		case MapDirection.NORTHEAST:
			switch (tile.directions [1]) {
			case MapDirection.NORTHWEST:
				return Quaternion.FromToRotation (Vector3.right, Vector3.left);
			case MapDirection.SOUTHEAST:
				return Quaternion.FromToRotation (Vector3.right, Vector3.forward);
			}
			break;
		case MapDirection.NORTHWEST:
			switch (tile.directions [1]) {
			case MapDirection.NORTHEAST:
				return Quaternion.FromToRotation (Vector3.right, Vector3.left);
			case MapDirection.SOUTHWEST:
				return Quaternion.FromToRotation (Vector3.right, Vector3.back);
			}
			break;
		case MapDirection.SOUTHEAST:
			switch (tile.directions [1]) {
			case MapDirection.NORTHEAST:
				return Quaternion.FromToRotation (Vector3.right, Vector3.forward);
			case MapDirection.SOUTHWEST:
				return Quaternion.identity;
			}
			break;
		case MapDirection.SOUTHWEST:
			switch (tile.directions [1]) {
			case MapDirection.NORTHWEST:
				return Quaternion.FromToRotation (Vector3.right, Vector3.back);
			case MapDirection.SOUTHEAST:
				return Quaternion.identity;
			}
			break;
		}
		return Quaternion.identity;
	}

	/*
	void instantiateCenter ()
	{
		GameObject go = PhotonNetwork.InstantiateSceneObject ("TestTile", Vector3.down, Quaternion.FromToRotation (Vector3.forward, Vector3.up), 0, null);
		go.transform.parent = _mapParent.transform;
		goPath [0] = go;
		((MeshRenderer)go.GetComponent ("MeshRenderer")).material.color = Color.black;
	}

	void generate (Tile[] path)
	{
		Vector3 pos = Vector3.down;
		for (int i=0; i<path.Length; i++) {
			pos = calculateNewPosition (path, i, pos);
			GameObject go = getCorrespondingObject (pos, path [i]);
			goPath [i + 1] = go;
			//GameObject go = (GameObject)Instantiate(Resources.Load("TestTileblend"), pos, Quaternion.FromToRotation(Vector3.forward, Vector3.up));
			//((MeshRenderer)go.transform.GetComponent("Cube_001").GetComponent("MeshRenderer")).material.color = new Color(0, 0, ((float) (i+1))/(float) path.Length);
		}
	}

	private Vector3 calculateNewPosition (Tile[] path, int i, Vector3 prevPos)
	{
		Vector3 newPos = new Vector3 ();
		newPos.x = prevPos.x;
		newPos.y = prevPos.y;
		newPos.z = prevPos.z;

		switch (path [i].start.direction) {
		case MapDirection.NORTH:
			newPos.x -= 9;
			break;
		case MapDirection.SOUTH:
			newPos.x += 9;
			break;
		case MapDirection.EAST:
			newPos.z += 9;
			break;
		case MapDirection.WEST:
			newPos.z -= 9;
			break;
		default:
			break;
		}
		return newPos;

	}

	private GameObject getCorrespondingObject (Vector3 pos, Tile tile)
	{
		GameObject go;
		switch (MapTools.isTurning (tile)) {
					
		case Turning.LEFT:
			go = PhotonNetwork.InstantiateSceneObject ("TestTileTurn", pos, getRotation (tile), 0, null);
			// go = (GameObject)Instantiate(Resources.Load("TestTileTurn"), pos, Quaternion.FromToRotation(Vector3.forward, Vector3.up));
			break;
		case Turning.RIGHT:
			go = PhotonNetwork.InstantiateSceneObject ("TestTileTurn", pos, getRotation (tile), 0, null);
			// go = (GameObject)Instantiate(Resources.Load("TestTileTurn"), pos, Quaternion.FromToRotation(Vector3.forward, Vector3.up));
			break;
		case Turning.STRAIGHT:
			go = PhotonNetwork.InstantiateSceneObject ("TestTileAhead", pos, getRotation (tile), 0, null);
			// go = (GameObject)Instantiate(Resources.Load("TestTileAhead"), pos, Quaternion.FromToRotation(Vector3.forward, Vector3.up));
			break;
		case Turning.DEADEND:
			go = PhotonNetwork.InstantiateSceneObject ("TestTile", pos, getRotation (tile), 0, null);
			break;
		default:
			go = PhotonNetwork.InstantiateSceneObject ("TestTile", pos, Quaternion.identity, 0, null);
			break;
		}
		go.transform.parent = _mapParent.transform;
		return go;
	}

	private Quaternion getRotation (Tile tile)
	{
		
		Quaternion rot = Quaternion.identity;
		switch (tile.start.direction) {
			
		case MapDirection.NORTH:
			switch (tile.arrival.direction) {
			case MapDirection.NOWHERE:
				rot = Quaternion.identity;
				break;
			case MapDirection.EAST:
				rot = Quaternion.FromToRotation (Vector3.back, Vector3.left);
				break;
			case MapDirection.WEST:
				rot = Quaternion.identity;
				break;
			case MapDirection.NORTH:
				rot = Quaternion.identity;
				break;
			case MapDirection.SOUTH:
				rot = Quaternion.identity;
				break;
			default:
				break;
			}		
			break;
		case MapDirection.SOUTH:
			switch (tile.arrival.direction) {
			case MapDirection.NOWHERE:
				rot = Quaternion.identity;
				break;		
			case MapDirection.EAST:
				rot = Quaternion.FromToRotation (Vector3.left, Vector3.right);
				break;
			case MapDirection.WEST:
				rot = Quaternion.FromToRotation (Vector3.right, Vector3.forward);
				break;
			case MapDirection.NORTH:
				rot = Quaternion.identity;
				break;
			case MapDirection.SOUTH:
				rot = Quaternion.identity;
				break;
			default:
				break;
			}		
			break;
		case MapDirection.EAST:
			switch (tile.arrival.direction) {
			case MapDirection.NOWHERE:
				rot = Quaternion.identity;	
				break;	
			case MapDirection.NORTH:
				rot = Quaternion.FromToRotation (Vector3.back, Vector3.left);
				break;
			case MapDirection.SOUTH:
				rot = Quaternion.FromToRotation (Vector3.right, Vector3.left);
				break;
			case MapDirection.EAST:
				rot = Quaternion.FromToRotation (Vector3.right, Vector3.forward);
				break;
			case MapDirection.WEST:
				rot = Quaternion.FromToRotation (Vector3.right, Vector3.forward);
				break;
			default:
				break;
			}		
			break;
		case MapDirection.WEST:
			switch (tile.arrival.direction) {
			case MapDirection.NOWHERE:
				rot = Quaternion.identity;		
				break;
			case MapDirection.NORTH:
				rot = Quaternion.identity;
				break;
			case MapDirection.SOUTH:
				rot = Quaternion.FromToRotation (Vector3.back, Vector3.right);
				break;
			case MapDirection.EAST:
				rot = Quaternion.FromToRotation (Vector3.right, Vector3.forward);
				break;
			case MapDirection.WEST:
				rot = Quaternion.FromToRotation (Vector3.right, Vector3.forward);
				break;
			default:
				break;
			}		
			break;
		default:
			break;
		}	

		rot = rot * Quaternion.FromToRotation(Vector3.back, Vector3.down);
		return rot;
	}

	// Update is called once per frame
	void Update ()
	{
	
	}
*/
}
