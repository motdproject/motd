
public enum TileType
{
	DECOR,
	SPAWN,
	PATH,
	SIDEWALK,
	BORDER,
	PLAYER_BASE
}

