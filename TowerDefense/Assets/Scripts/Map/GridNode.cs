﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class GridNode
{
	public int xIndex, zIndex;
	public Vector3 position;

	public GridNode (int indexX, int indexZ, float x, float y, float z)
	{
		xIndex = indexX;
		zIndex = indexZ;
		position = new Vector3 (x, y, z);
	}
}
