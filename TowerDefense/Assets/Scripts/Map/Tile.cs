﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Tile {

	public List<MapDirection> directions;
	public TileType type;

	public Tile() {
		directions = new List<MapDirection>();
	}

	public override string ToString ()
	{
		string tile = "(";
		for (int i=0; i<directions.Count; i++) {
			tile += "" + directions[i] + ";";
		}
		tile += ")";

		return tile;
	}
	
}
