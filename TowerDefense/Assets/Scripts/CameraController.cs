using UnityEngine;
using System.Collections;

// 0 35 -50
public class CameraController : MonoBehaviour
{
	public GameObject Player {
		get {
			return _player;
		}
		set {
			_player = value;
			QuickUpdateCamera ();
		}
	}

	// Radius: the distance from the origin to the point
	public float distanceToPlayer;
	// Elevation angle: the angle between the plane (with zenith axis as normal) and the line from the origin to the point
	public float elevation;
	// Polar angle: the rotation around the zenith axis
	public float rotation;
	public float rotationSensitivity = 0.2f;
	public float zoomSensitivity = 5f;
	public float cameraSpeed;
	public float damping;

	private SphericalCoordinates _sc;
	private GameObject _player;

	void Start ()
	{
		_sc = new SphericalCoordinates (distanceToPlayer, rotation, elevation, 1F, 200F);
	}

	void LateUpdate ()
	{
		if (Input.GetMouseButton (2)) {
			rotation += -Input.GetAxis ("Mouse X") * rotationSensitivity;
			elevation += -Input.GetAxis ("Mouse Y") * rotationSensitivity;
			QuickUpdateCamera ();
		} else if (Input.GetAxis ("Mouse ScrollWheel") != 0) {
			distanceToPlayer += -Input.GetAxis ("Mouse ScrollWheel") * zoomSensitivity;
			QuickUpdateCamera ();
		} else if (Player) {
			// Smooth movement to where the player is
			Vector3 targetPosition = Player.transform.position + _sc.toCartesian;
			transform.position = Vector3.Lerp (transform.position, targetPosition, Time.deltaTime * cameraSpeed);

			//Look at and dampen the rotation
			Quaternion rotation = Quaternion.LookRotation (Player.transform.position - transform.position);
			transform.rotation = Quaternion.Slerp (transform.rotation, rotation, Time.deltaTime * damping);
		}
	}

	void QuickUpdateCamera ()
	{
		_sc = new SphericalCoordinates (distanceToPlayer, rotation, elevation, 1F, 100F);
		transform.position = Player.transform.position + _sc.toCartesian;
		transform.LookAt (Player.transform);
	}
}
