﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class PlayerDeath : MonoBehaviour
{
	public string spawnerName = "Spawner";
	public float respawnRate;
	
	[SerializeField]
	private Material[]
		_originalMaterials;
	[SerializeField]
	private Material[]
		_deathMaterials;
	private SkinnedMeshRenderer _renderer;

	void Start ()
	{
		_renderer = GetComponentInChildren<SkinnedMeshRenderer> ();
		int nbOfMaterials = _renderer.materials.Length;

		_originalMaterials = new Material[nbOfMaterials];
		for (int i = 0; i < nbOfMaterials; i++) {
			_originalMaterials [i] = _renderer.materials [i];
		}

		_deathMaterials = new Material[nbOfMaterials];
		for (int i = 0; i < nbOfMaterials; i++) {
			_deathMaterials [i] = Instantiate (_renderer.materials [i]) as Material;
			_deathMaterials [i].SwitchRenderModeToFade ();
			_deathMaterials [i].SetColor ("_Color",
									new Color (_deathMaterials [i].color.r, 
									          _deathMaterials [i].color.g, 
									          _deathMaterials [i].color.b, 
									          _deathMaterials [i].color.a / 3));
		}
	}

	public void Die ()
	{
		//Debug.Log ("Dying");
		Respawn ();
		gameObject.GetPhotonView ().RPC ("Ghost", PhotonTargets.AllBuffered);
		ShowCountDown ();
	}

	public void Revive ()
	{
		UnGhost ();
	}

	void Respawn ()
	{
		//Debug.Log ("Respawning");
		Transform spawner = GameObject.FindGameObjectWithTag (TagNames.PLAYER_BASE).transform.Find (spawnerName);
		//Debug.Log ("Spawner is " + spawner + " at position " + spawner.position);
		gameObject.transform.position = spawner.position;
	}

	[RPC]
	void Ghost ()
	{
		//Debug.Log ("Taking a ghastly appearance");
		_renderer.materials = _deathMaterials;
		Camera.main.GetComponent<Grayscale> ().enabled = true;
	}

	void ShowCountDown ()
	{
		Messenger<float>.Broadcast (EventNames.SHOW_COUNTDOWN, respawnRate);
	}

	void UnGhost ()
	{
		//Debug.Log ("Back to normal");
		_renderer.materials = _originalMaterials;
		Camera.main.GetComponent<Grayscale> ().enabled = false;
	}
}
