﻿using UnityEngine;
using System;
using System.Collections;

using MonsterLove.StateMachine;

public class Player : MonsterLove.StateMachine.StateMachineBehaviour
{

	public enum States
	{
		Idle,
		Moving,
		PreparingToBuild,
		Building,
		Attacking,
		Dead
	}

	/*public Player.States CurrentState {
		get {
			if (_photonView.isMine) {
				return (Player.States)GetState ();
			} else {
				return _copiedState;
			}
		}
		set {
			if (_photonView && _photonView.isMine) {
				ChangeState (value);
			} else {
				_copiedState = value;
			}
		}
	}*/

	private PlayerClickMovement _movement;
	private PlayerConstruction _construction;
	private PlayerAttack _attack;
	private PlayerDeath _death;
	private PlayerHealth _health;
	private PhotonView _photonView;
	private Animator _animator;

	private float _deathTime;
	//private States _copiedState;

	void Awake ()
	{		
		//Initialize State Machine Engine
		Initialize<States> ();

	}

	void Start ()
	{
		_movement = GetComponent < PlayerClickMovement> ();
		_construction = GetComponent<PlayerConstruction> ();
		_attack = GetComponent<PlayerAttack> ();
		_photonView = GetComponent<PhotonView> ();
		_health = GetComponent<PlayerHealth	> ();
		_animator = GetComponentInChildren<Animator> ();
		_death = GetComponent<PlayerDeath> ();
		//Change to our first state
		ChangeState (States.Idle);
	}

	void Idle_Update ()
	{
		//_animator.SetBool("isRunning", false);
		if (_photonView.isMine) {
			_construction.HandleInputs ();
			_movement.HandleInputs ();
		}
	}

	void Moving_Enter ()
	{
		_animator.SetBool ("isRunning", true);
	}

	void Moving_Update ()
	{
		if (_photonView.isMine) {
			_construction.HandleInputs ();
			_movement.HandleInputs ();
		}
		_movement.Moving ();

	}

	void Moving_FixedUpdate ()
	{
		_movement.FixedMoving ();
		
	}

	void Moving_Exit ()
	{
		_animator.SetBool ("isRunning", false);
	}

	void Attacking_Enter ()
	{
		if (_photonView.isMine) {
			_attack.Attack ();
		}
	}

	void Attacking_Update ()
	{
		if (_photonView.isMine) {
			_construction.HandleInputs ();
			_movement.HandleInputs ();
		}
		_attack.Attack ();
	}

	void PreparingToBuild_Update ()
	{
		if (_photonView.isMine)
			_construction.HandleInputs ();
	}

	void Building_Enter ()
	{
		_animator.SetBool ("isBuilding", true);
		if (_photonView.isMine) {
			_construction.Pay ();
		}
	}

	void Building_Update ()
	{
		if (_photonView.isMine) {
			//_movement.HandleInputs ();
			_construction.Build ();
		}
	}

	void Building_Exit ()
	{
		_animator.SetBool ("isBuilding", false);
		
		if (_photonView.isMine) {
			_construction.EndConstruction ();
		}
	}

	void Dead_Enter ()
	{
		if (_photonView.isMine) {
			_deathTime = Time.time;
			_death.Die ();
		}
	}

	void Dead_Update ()
	{
		if (_photonView.isMine) {
			// TODO: Visual indicator
			if (Time.time > _deathTime + _death.respawnRate) {
				ChangeState (States.Idle);
			}
		}
	}

	void Dead_Exit ()
	{
		if (_photonView.isMine) {
			_death.Revive ();
			_health.photonView.RPC ("Reset", PhotonTargets.All);
		}
	}

	public void MoveTo (Vector3 destination)
	{
		if (_photonView.isMine) {
			_movement.MoveTo (destination);
		}
	}


	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			//Debug.Log ("Sending my state");
			stream.SendNext ((byte)((States)GetState ()));
		} else {
			object next = stream.ReceiveNext ();
			//Debug.Log ("Storing network state");
			//Debug.Log (next.GetType ());
			//Debug.Log (next);
			States state = (States)Enum.Parse (typeof(States), "" + next);
			//Debug.Log (state);
			ChangeState (state);
			//CurrentState = ((States)stream.ReceiveNext ());
		}
	}
	
	[RPC]
	public void Buff (int idBooster, int playerId, byte stat, float multiplier)
	{
		//Debug.Log ("Receiving buff for " + playerId + " and we're " + photonView.viewID + " from " + idBooster);
		if (playerId != photonView.viewID) {
			//Debug.Log ("Receiving buff for " + playerId + " but we're " + photonView.viewID + " so that's not for us");
			return;
		}
		switch ((Buff.Stat)stat) {
		case global::Buff.Stat.HP:
			GetComponent<PlayerHealth> ().Buff (idBooster, multiplier);
			break;
		default:
			break;
		}

	}
	
	[RPC]
	public void Debuff (int idBooster, int playerId, byte stat)
	{
		//Debug.Log ("Receiving debuff for " + playerId + " and we're " + photonView.viewID + " from " + idBooster);
		if (playerId != photonView.viewID)
			return;

		switch ((Buff.Stat)stat) {
		case global::Buff.Stat.HP:
			GetComponent<PlayerHealth> ().Debuff (idBooster);
			break;
		default:
			break;
		}
	}

	[RPC]
	void AllocatePlayerIndex (int index)
	{
		GetComponentInChildren<OwnerIndicator> ().PlayerIndex = index;
	}

	public void DoChangeState (States newState)
	{
		if (GetState () != null && newState == ((Player.States)GetState ()))
			return;

		if (_animator) {
			_animator.SetBool ("isBuilding", false);
			_animator.SetBool ("isRunning", false);
		}
		//Debug.Log ("Changing state to " + newState);
		ChangeState (newState);
	}
}
