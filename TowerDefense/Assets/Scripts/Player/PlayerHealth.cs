﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerHealth : Photon.MonoBehaviour
{
	public float maxHp, currentHp;
	
	private Dictionary<int, float> buffs = new Dictionary<int, float> ();

	private Player _player;

	void Start ()
	{
		_player = GetComponent<Player> ();

		//Messenger<int, float, float>.Broadcast (EventNames.HEALTH_CHANGED, gameObject.GetPhotonView ().viewID, currentHp, maxHp);
	}

	[RPC]
	void TakeDamage (float amount)
	{
		currentHp -= amount;
		if (currentHp <= 0) {
			//Debug.Log ("Dieeeee");
			_player.DoChangeState (Player.States.Dead);
			//currentHp = maxHp;
		}
		//Messenger<int, float, float>.Broadcast (EventNames.HEALTH_CHANGED, gameObject.GetPhotonView ().viewID, currentHp, maxHp);
	}

	[RPC]
	void Reset ()
	{
		currentHp = maxHp;
	}

	public void Buff (int idBooster, float multiplier)
	{
		if (!buffs.ContainsKey (idBooster)) {
			//Debug.Log ("Adding " + idBooster + " to boosts for player " + _player.gameObject.GetPhotonView ().viewID);
			buffs.Add (idBooster, multiplier);
			//Debug.Log (buffs.ToStringFull ());
			maxHp *= multiplier;
			currentHp *= multiplier;
			//Messenger<int, float, float>.Broadcast (EventNames.HEALTH_CHANGED, gameObject.GetPhotonView ().viewID, currentHp, maxHp);
		}
	}

	public void Debuff (int idBooster)
	{
		//Debug.Log ("Removing " + idBooster + " from boosts for player " + _player.gameObject.GetPhotonView ().viewID);
		maxHp /= buffs [idBooster];
		currentHp /= buffs [idBooster];
		buffs.Remove (idBooster);
		//Debug.Log (buffs.ToStringFull ());
		//Messenger<int, float, float>.Broadcast (EventNames.HEALTH_CHANGED, gameObject.GetPhotonView ().viewID, currentHp, maxHp);
	}

	// TODO: should probably send this via RPC when it changes rather than sending it continuously
	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			//Debug.Log ("Sending my state");
			stream.SendNext (currentHp);
			stream.SendNext (maxHp);
		} else {
			currentHp = (float)stream.ReceiveNext ();
			maxHp = (float)stream.ReceiveNext ();
		}
	}
}
