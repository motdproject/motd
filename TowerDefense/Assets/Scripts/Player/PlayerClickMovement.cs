﻿using UnityEngine;
using System.Collections;

public class PlayerClickMovement : Photon.MonoBehaviour
{  
	public float moveSpeed = 100;
	public float maximumRotationSpeed = 400f;
	// TODO: compute that automatically? Don't compute the distance from the center of the player but from their feet?
	public float distanceFromCenterToGround = 1.25f;

	private CharacterController _controller;
	private Transform _transform;
	public Quaternion Rotation;
	private Player _player;
	private Camera _mainCamera;
	private PlayerAttack _attack;
	private PlayerConstruction _construction;
	private Rigidbody _rigidbody;

	private Vector3 _destination;
	private float _velocity;
	private Vector3 _lookPosition;
	
	void Awake ()
	{
		_controller = GetComponent<CharacterController> ();
		_attack = GetComponent<PlayerAttack> ();
		_construction = GetComponent<PlayerConstruction> ();
		_transform = transform;
		_player = GetComponent<Player> ();
		_rigidbody = GetComponent<Rigidbody> ();
		_mainCamera = Camera.main;
	}

	void Start ()
	{
		_destination = Vector3.zero;
	}

	public void HandleInputs ()
	{
		if (Input.GetMouseButton (0)) {
			// We need to actually hit an object
			RaycastHit hit;
			Ray ray = _mainCamera.ScreenPointToRay (Input.mousePosition);
			
			if (!Physics.Raycast (_mainCamera.transform.position, ray.direction, out hit, Mathf.Infinity)) {
				return;
			}

			// Attacking an enemy
			string targetTag = hit.collider.tag;
			if (TagNames.ENEMY == targetTag) {
				// Trigger the attack
				GameObject enemy = hit.transform.gameObject;
				_attack.target = enemy;
			} else {
				_attack.target = null;
			}

			_construction.Cancel ();

			MoveTo (hit.point);
		}
	}

	public void MoveTo (Vector3 destination)
	{
		_destination = destination;
		_player.DoChangeState (Player.States.Moving);
		
	}

	public void Moving ()
	{
		if (_attack.target) {
			_destination = _attack.target.transform.position;
		}

		//Debug.Log ("Handling move");
		float deltaTime = Time.deltaTime;
		//Debug.Log ("Moving towards " + _destination);
		_lookPosition = _destination - _transform.position;

		// Consider the player as arrived to destination once they are close enough
		// TODO: probably a better way to do this
		if (_attack.Notify ()) {
			return;
		}
		if (_construction.Notify ()) {
			return;
		}
		if (_lookPosition.sqrMagnitude < distanceFromCenterToGround) {
			_transform.position = _destination;
			_destination = Vector3.zero;
			_player.DoChangeState (Player.States.Idle);
			_velocity = 0;
			//Debug.Log ("Player arrived");
			return;
		}

	}

	public void FixedMoving ()
	{
		float deltaTime = Time.deltaTime;
		// We don't want the speed to be influenced by how far away from the player we click
		Vector3 direction = Vector3.Normalize (_lookPosition);
		//Debug.DrawLine (_transform.position, _destination);
		//Debug.Log ("Moving by " + direction * deltaTime * moveSpeed);
		//Debug.Log ("MFar away? " + lookPosition.sqrMagnitude);
		//Debug.Log ("Moving towards " + _destination);
		_controller.SimpleMove (direction * deltaTime * moveSpeed);
		
		// TODO: handle rotation
		Quaternion look = Quaternion.LookRotation (_lookPosition);
		Vector3 newRotation = look.eulerAngles;
		Vector3 angles = _transform.rotation.eulerAngles;
		float newY = Mathf.SmoothDampAngle (angles.y, newRotation.y, ref _velocity, 0, maximumRotationSpeed, deltaTime);
		Quaternion res = Quaternion.Euler (angles.x, newY, angles.z);
		_transform.rotation = res;
	}

	void Update ()
	{
		if (!photonView.isMine) {
			//if (_syncTime / _syncDelay < 1) {
			//_syncTime += Time.deltaTime;

			if (Time.time - _lastSynchronizationTime > positionRefreshRate) {
				_transform.position = _syncPosition;
				_transform.rotation = _syncRotation;
				_lastSynchronizationTime = Time.time;
			}
			if (Vector3.Distance (_destination, Vector3.zero) < 0.05) {
				_player.DoChangeState (Player.States.Idle);
			} else {
				MoveTo (_destination);
			}
		}
	}

	public float positionRefreshRate = 10;
	private float _lastSynchronizationTime = 0f;
	private Vector3 _syncPosition;
	private Quaternion _syncRotation;
	
	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			stream.SendNext (_transform.position);
			stream.SendNext (_destination);
			stream.SendNext (_transform.rotation);

		} else {
			_syncPosition = (Vector3)stream.ReceiveNext ();
			_destination = (Vector3)stream.ReceiveNext ();
			_syncRotation = (Quaternion)stream.ReceiveNext ();
		}
	}
}
