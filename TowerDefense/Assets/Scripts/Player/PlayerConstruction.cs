﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class PlayerConstruction : MonoBehaviour
{
	public List<PrefabKey> towerPrefabs = new List<PrefabKey> ();
	public GameObject ongoingConstructionPrefab;
	public GameObject towerProxyPrefab;
	public float distanceToConstruction;
	public float clickTimeTolerance;
	
	private Player _player;
	private PhotonView _photonView;
	private Gold _teamGold;
	private Camera _mainCamera;

	private OngoingConstruction _tempTower;
	private TowerProxy _towerProxy;
	private GameObject _tower;
	private float _constructionEndTime;
	private Vector3 _constructionPoint;
	private float _lastClickTime;

	void Start ()
	{
		_player = GetComponent<Player> ();
		_photonView = GetComponent<PhotonView> ();
		_teamGold = GameObject.FindGameObjectWithTag (TagNames.PLAYER_BASE).GetComponentInChildren<Gold> ();
		_mainCamera = Camera.main;
	}

	public void HandleInputs ()
	{
		foreach (PrefabKey entry in towerPrefabs) {
			if (Input.GetKeyDown (entry.key) && !_towerProxy) {
				// http://answers.unity3d.com/questions/555101/possible-to-make-gameobjectgetcomponentinchildren.html
				if (_teamGold.CanPay (entry.prefab.GetComponentsInChildren<Tower> (true) [0].price)) {
					_tower = entry.prefab;
					_towerProxy = GameObject.Instantiate (towerProxyPrefab).GetComponent<TowerProxy> ();
					_towerProxy.Tower = _tower;
					_player.DoChangeState (Player.States.PreparingToBuild);
				}
			}
		}

		if (_towerProxy && _towerProxy.canBuild && Input.GetMouseButton (0)) {
			_lastClickTime = Time.time;
			// We need to actually hit an object
			RaycastHit hit;
			Ray ray = _mainCamera.ScreenPointToRay (Input.mousePosition);
			
			if (!Physics.Raycast (_mainCamera.transform.position, ray.direction, out hit, Mathf.Infinity)) {
				return;
			}
			
			_constructionPoint = hit.point;
			_towerProxy.NotifyConstructionChosen ();

			_player.MoveTo (_constructionPoint);
		}
		
		if (_towerProxy && Input.GetKeyDown (KeyCode.Escape)) {
			_towerProxy.RemoveProxy ();
			_player.DoChangeState (Player.States.Idle);
		}
	}

	public void Cancel ()
	{
		if (_towerProxy && Time.time > _lastClickTime + clickTimeTolerance) {
			_towerProxy.RemoveProxy ();
		}
	}

	public bool Notify ()
	{
		//Debug.Log ("Notifying construction");
		if (!_towerProxy)
			return false;

		//Debug.Log ("And there is a tower proxy");
		var targetVector = _constructionPoint - transform.position;
		
		if (targetVector.sqrMagnitude > distanceToConstruction * distanceToConstruction)
			return false;

		//Debug.Log ("And we're close enough");
		_towerProxy.RemoveProxy ();
		_player.DoChangeState (Player.States.Building);
		return true;
	}

	public void Pay ()
	{
		//Debug.Log ("Paying");
		// TODO: this doesn't prevent edge cases when two players construct at exactly the same time
		//teamGold.GetComponent<PhotonView>().RPC("Pay", PhotonTargets.AllViaServer, constructionCost);
		Tower tower = _tower.GetComponentsInChildren<Tower> (true) [0];
		if (_teamGold.Pay (tower.price)) {
			_player.DoChangeState (Player.States.Building);
			_constructionEndTime = Time.time + tower.buildDuration;
			//Debug.Log ("Start : " + Time.time + " ; End : " + _constructionEndTime);
			object[] data = new object[2];
			data [0] = _tower.name;
			data [1] = tower.buildDuration;
			GameObject networkTower = PhotonNetwork.Instantiate (ongoingConstructionPrefab.name, _constructionPoint, Quaternion.identity, 0, data);
			_tempTower = networkTower.GetComponent<OngoingConstruction> ();
			//_tempTower.Tower = _tower;
			//_tempTower.totalConstructionTime = tower.buildDuration;
		} else {
			_player.DoChangeState (Player.States.Idle);
		}

	}

	public void Build ()
	{
		if (Time.time > _constructionEndTime && _tower) {
			_photonView.RPC ("SpawnTower", PhotonTargets.MasterClient, _tower.GetComponentsInChildren<Tower> (true) [0].id, _constructionPoint, Quaternion.identity);
			_player.DoChangeState (Player.States.Idle);
		}
	}

	public void EndConstruction ()
	{
		if (_tempTower) {
			PhotonNetwork.Destroy (_tempTower.gameObject);
		}
	}

	GameObject getTowerPrefab (string id)
	{
		foreach (PrefabKey entry in towerPrefabs) {
			Tower tower = entry.prefab.GetComponentsInChildren<Tower> (true) [0];
			if (id == tower.id) {
				return entry.prefab;
			}
		}
		return null;
	}

	[RPC]
	public void SpawnTower (string id, Vector3 position, Quaternion rotation)
	{
		GameObject prefab = getTowerPrefab (id);
		PhotonNetwork.InstantiateSceneObject (prefab.name, position, rotation, 0, null);
		Messenger.Broadcast (EventNames.MAP_CHANGED);	
	}

	[Serializable]
	public class PrefabKey
	{
		public GameObject prefab;
		public KeyCode key;
	}
}
