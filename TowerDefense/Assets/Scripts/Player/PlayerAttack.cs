﻿using UnityEngine;
using System.Collections;

// TODO: make attacking more "challenging": click & attack a single enemy at a time
public class PlayerAttack : Photon.MonoBehaviour
{
	public float attackRadius = 10f;
	public float closingRange = 1f;
	public float attackStrength = 5;
	public float attackSpeed = 1;
	public GameObject target;

	private Player _player;

	private Hashtable boosts = new Hashtable ();

	private float _lastAttackTime;
	
	void Start ()
	{
		_player = GetComponent<Player> ();

		_lastAttackTime = Time.time;
	}

	public bool Notify ()
	{
		if (!target)
			return false;

		var targetVector = target.transform.position - transform.position;

		if (targetVector.sqrMagnitude > (attackRadius - closingRange) * (attackRadius - closingRange))
			return false;

		_player.DoChangeState (Player.States.Attacking);
		return true;
	}
	
	public void Attack ()
	{
		if (!target) {
			_player.DoChangeState (Player.States.Idle);
			return; 
		}

		// If too far, go back to sleep
		var targetVector = target.transform.position - transform.position;
		if (targetVector.sqrMagnitude > attackRadius * attackRadius) {
			_player.DoChangeState (Player.States.Moving);
			return; 
		}

		if (Time.time > _lastAttackTime + attackSpeed) {
			_lastAttackTime = Time.time;
			float damage = attackStrength;
			foreach (float b in boosts.Values) {
				damage = b * damage;
			}
			Debug.Log ("Dealing " + damage + " damage");
			target.GetPhotonView ().RPC ("TakeDamage", PhotonTargets.All, damage);
		}
	}

	public void Boost (int idBooster, float value)
	{
		if (!boosts.ContainsKey (idBooster)) {
			boosts.Add (idBooster, value);
		}
	}

	public void UnBoost (int idBooster)
	{
		boosts.Remove (idBooster);
	}
}
