﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OwnerIndicator : MonoBehaviour
{
	public List<Color> playerColors = new List<Color> ();

	public int PlayerIndex {
		get {
			return _index;
		}
		set {
			_index = value;
			transform.parent.GetComponentInChildren<AreaOfEffect> ().MainColor = playerColors [_index];
		}
	}

	private int _index;
}
