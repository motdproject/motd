﻿using UnityEngine;
using System.Collections;

public class EnemyMove : Photon.MonoBehaviour
{
	public bool slowed = false;
	private EnemyBehaviour _behavior;

	void Start ()
	{
		_behavior = GetComponent<EnemyBehaviour> ();
	}
		
	public void Slow ()
	{
		if (!slowed) {
			_behavior.moveSpeed = _behavior.moveSpeed / 2;
			slowed = true;
		}
	}

	public void UnSlow ()
	{
		if (slowed) {
			_behavior.moveSpeed = _behavior.moveSpeed * 2;
			slowed = false;
		}
	}



}
