﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyBehaviour : Photon.MonoBehaviour
{
	// public GameObject target;

	public float moveSpeed;
	public float rotationSpeed;
	public EnemySpawner originBase;
	// TODO: compute this automatically
	public float heightOfCenterOfGravity;

	private CharacterController _controller;
	private FindTarget _targetter;
	private EnemyAttack _attack;
	private EnemyHealth _health;

	private int _pathIndex;
	private Vector3 _lookPosition;

	// Enemy faces the right direction
	void Start ()
	{
		_controller = GetComponent<CharacterController> ();
		_targetter = GetComponentInChildren<FindTarget> ();
		_attack = GetComponent<EnemyAttack> ();
		_health = GetComponent<EnemyHealth> ();

		object[] data = GetComponent<PhotonView> ().instantiationData;
		originBase = PhotonView.Find ((int)data [0]).gameObject.GetComponent<EnemySpawner> ();

		// Update stats based on difficulty
		float difficultyMultiplier = (float)data [1];
		_attack.Scale (difficultyMultiplier);
		_health.Scale (difficultyMultiplier);
	}

	void Update ()
	{
		if (PhotonNetwork.isMasterClient) {
			//UpdateTarget ();
			ControlledMovement ();  
			AttackPlayer ();
		} else {
			SyncedMovement ();
		}
	}

	void AttackPlayer ()
	{
		_attack.Attack ();
	}
	
	// Move
	void ControlledMovement ()
	{
		List<Vector3> Path = originBase.GetComponent<EnemySpawner> ().pathToTarget;
		if (Path.Count == 0) {
			Debug.LogWarning ("No path to target");
			return;
		}
		if (Path.Count <= _pathIndex) {
			Debug.LogWarning ("Path index > Path.Count");
			return;
		}

		Vector3 target = new Vector3 (Path [_pathIndex].x, transform.position.y, Path [_pathIndex].z);
		//Debug.Log ("Going towards " + target);
		//Debug.Log (Path);
		_lookPosition = target - transform.position;

		if (Vector3.Distance (transform.position, target) < (heightOfCenterOfGravity + .1f)) {
			_pathIndex++;
		}
		
		//look at
		transform.rotation = Quaternion.Slerp (transform.rotation, 
		                                       Quaternion.LookRotation (target - transform.position), 
		                                       rotationSpeed * Time.deltaTime);
	}

	void FixedUpdate ()
	{
		if (PhotonNetwork.isMasterClient) {
			FixedControlledMovement ();
		}
	}

	void FixedControlledMovement ()
	{
		Vector3 direction = Vector3.Normalize (_lookPosition);
		//Debug.Log ("Direction is " + direction);
		_controller.SimpleMove (direction * Time.deltaTime * moveSpeed);
	}
	
	private void SyncedMovement ()
	{
		syncTime += Time.deltaTime;
		transform.position = Vector3.Lerp (syncStartPosition, syncEndPosition, syncTime / syncDelay);
	}
	
	public float lastSynchronizationTime = 0f;
	public float syncDelay = 0f;
	public float syncTime = 0f;
	public Vector3 syncStartPosition;
	public Vector3 syncEndPosition;
	
	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			stream.SendNext (transform.position);
			stream.SendNext (_controller.velocity);
		} else {
			Vector3 syncPosition = (Vector3)stream.ReceiveNext ();
			Vector3 syncVelocity = (Vector3)stream.ReceiveNext ();
			
			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;
			
			syncEndPosition = syncPosition + syncVelocity * syncDelay;
			syncStartPosition = transform.position;
		}
	}
}
