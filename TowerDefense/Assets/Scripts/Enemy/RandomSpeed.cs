﻿using UnityEngine;
using System.Collections;

public class RandomSpeed : MonoBehaviour
{
	private float _updateSpeedTime;
	
	private EnemyBehaviour _behavior;
	
	void Start ()
	{
		_behavior = GetComponent<EnemyBehaviour> ();
		_updateSpeedTime = Time.time + 1;	
	}

	void Update ()
	{
		if (PhotonNetwork.isMasterClient) {
			UpdateSpeed ();  
		}
	}

	void UpdateSpeed ()
	{
		if (Time.time > _updateSpeedTime) {
			_behavior.moveSpeed = 300 + ((0.5f - Random.value) * 300);
			_updateSpeedTime = Time.time + 1;
		}
		
	}
}
