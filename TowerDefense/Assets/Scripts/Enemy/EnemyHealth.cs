﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : Photon.MonoBehaviour
{
	public float hp;

	[RPC]
	public void TakeDamage (float amount)
	{
		Debug.Log ("Taking " + amount + " damage");
		hp -= amount;
		if (hp <= 0) {
			// Don't wait for the info to go over the network again
			Destroy (gameObject);

			// Only the master client (more or less the server) will trigger events based on death
			if (PhotonNetwork.isMasterClient) {
				// Send message that an enemy has died
				Messenger<GameObject>.Broadcast (EventNames.ENEMY_KILLED, gameObject);	
			}
		}
	}
	
	
	public void Scale(float factor) {
		this.hp *= factor;
	}


}
