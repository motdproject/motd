﻿using UnityEngine;
using System.Collections;

public class FindTarget : MonoBehaviour
{
	public GameObject target;
	public string defaultTargetNameTag = TagNames.PLAYER_BASE;
	public GameObject defaultTarget;

	void Start ()
	{
		defaultTarget = GameObject.FindWithTag (defaultTargetNameTag);
	}

	// Only target players who are building something
	/*void Update ()
	{
		if (target && target.tag == "Player") {
			Player player = target.GetComponent<Player> ();
			if (!Player.States.Building.Equals (player.GetState ())) {
				target = null;
			}
		}
	}*/

	public GameObject ComputeTarget ()
	{
		if (!target) {
			target = defaultTarget;
		}
		//Debug.Log ("Target is " + target);
		return target;
	}

	/*void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player") {
			Player player = other.GetComponent<Player> ();
			if (Player.States.Building.Equals (player.GetState ())) {
				target = other.gameObject;
			}
		}
	}

	void OnTriggerStay (Collider other)
	{
		if (other.tag == "Player") {
			Player player = other.GetComponent<Player> ();
			if (Player.States.Building.Equals (player.GetState ())) {
				target = other.gameObject;
			}
		}
	}*/
}
