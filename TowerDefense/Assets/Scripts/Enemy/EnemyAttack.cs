﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{

	public float attackRadius = 10f;
	//public float closingRange = 1f;
	public float attackStrength = 5;
	public float attackSpeed = 1;
	
	private float _lastAttackTime;
	
	void Start ()
	{		
		_lastAttackTime = Time.time;
	}
	
	public void Attack ()
	{		
		if (Time.time > _lastAttackTime + attackSpeed) {
			Collider[] hitColliders = Physics.OverlapSphere (this.transform.position, attackRadius);
			int i = 0;
			while (i < hitColliders.Length) {
				GameObject target = hitColliders [i].gameObject;
				if (target && target.tag == TagNames.PLAYER && hitColliders [i] is CharacterController) {
					Player player = target.GetComponent<Player> ();
					if (Player.States.Building.Equals (player.GetState ())) {
						Debug.Log ("Attacking " + hitColliders);
						target.GetPhotonView ().RPC ("TakeDamage", PhotonTargets.All, attackStrength);
					}
				}
				i++;
			}
		
			_lastAttackTime = Time.time;
		}
	}

	public void Scale(float factor) {
		this.attackStrength *= factor;
	}
}
