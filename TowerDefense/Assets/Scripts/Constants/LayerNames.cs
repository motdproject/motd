﻿using UnityEngine;
using System.Collections;

public class LayerNames
{
	public const string TOWER = "Entity";
	public const string PLAYER = "Player";
	public const string ENEMY = "Enemy";
}
