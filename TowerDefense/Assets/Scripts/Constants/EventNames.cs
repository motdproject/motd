﻿using UnityEngine;
using System.Collections;

public class EventNames
{
	public const string SERVER_CREATED = "server created";
	public const string MAP_CHANGED = "map changed";
	public const string MAP_GENERATED = "map generated";
	public const string MAP_READY = "map ready";
	public const string ENEMY_KILLED = "enemy killed";
	public const string HEALTH_CHANGED = "health changed";
	public const string PLAYER_SPAWNED = "player spawned";
	public const string SHOW_COUNTDOWN = "show countdown";
	public const string SPAWNER_CREATED = "spawner created";
	public const string WAVE_FINISHED = "wave finished";
}
