﻿using UnityEngine;
using System.Collections;

public class TagNames
{
	public const string TOWER = "Entity";
	public const string PLAYER = "Player";
	public const string ENEMY = "Enemy";
	public const string PLAYER_BASE = "Player base";
	public const string CONSTRUCTIBLE = "Constructible";
	public const string TILE = "Tile";
	public const string DECOR = "Decor";
}
