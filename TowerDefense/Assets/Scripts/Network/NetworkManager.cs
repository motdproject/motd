﻿using UnityEngine;
using System;
using System.Collections;

// http://www.paladinstudios.com/2014/05/08/how-to-create-an-online-multiplayer-game-with-photon-unity-networking/
public class NetworkManager : MonoBehaviour
{
	public PlayerSpawn spawner;
	
	private const string roomName = "RoomName";
	private RoomInfo[] roomsList;
	
	void Awake ()
	{
		Network.sendRate = 25;
		Time.timeScale = 0;
	}
	
	// Use this for initialization
	void Start ()
	{
		PhotonNetwork.ConnectUsingSettings ("0.1");
	}
	
	void OnGUI ()
	{
		if (!PhotonNetwork.connected) {
			GUILayout.Label (PhotonNetwork.connectionStateDetailed.ToString ());
		} else if (PhotonNetwork.room == null) {
			// Create Room
			if (GUI.Button (new Rect (100, 100, 250, 100), "Start Server"))
				StartServer ();
			
			// Join Room
			if (roomsList != null) {
				for (int i = 0; i < roomsList.Length; i++) {
					if (GUI.Button (new Rect (100, 250 + (110 * i), 250, 100), "Join " + roomsList [i].name))
						PhotonNetwork.JoinRoom (roomsList [i].name);
				}
			}
		}
	}
	
	void OnReceivedRoomListUpdate ()
	{
		roomsList = PhotonNetwork.GetRoomList ();
	}
	
	void OnJoinedRoom ()
	{
		//gameObject.AddComponent<NetworkSerialization> ();
		NetworkSerialization.Register ();
		Debug.Log ("Connected to Room");
		Time.timeScale = 1;
		// Map already created
		if (!PhotonNetwork.isMasterClient) {
			Debug.Log ("Joined - Retrieving all objects");
			// Wait until spawner is loaded
			StartCoroutine (StartLoader ());
		} 
		// Need to build the map before spawning player
		else {
			Messenger.Broadcast (EventNames.SERVER_CREATED);	
		}
	}

	IEnumerator StartLoader ()
	{
		GameObject playerBase = GameObject.FindGameObjectWithTag (TagNames.PLAYER_BASE);
		if (playerBase == null) {
			yield return new WaitForSeconds (0.1f);
			StartCoroutine (StartLoader ());
		} else {
			playerBase.GetComponentInChildren<PlayerSpawn> ().Spawn ();
		}
	}
	
	void StartServer ()
	{
		// Create the server
		PhotonNetwork.CreateRoom (roomName + Guid.NewGuid ().ToString ("N"), new RoomOptions () { maxPlayers = 10}, null);
	}
}
