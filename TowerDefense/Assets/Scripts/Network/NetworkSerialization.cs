﻿using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.IO;
using ExitGames.Client.Photon;

internal static class NetworkSerialization
{	
	internal static void Register ()
	{
		Debug.Log ("Registering custom enums");
		PhotonPeer.RegisterType (typeof(Tile), (byte)'A', SerializeTile, DeserializeTile);
		Debug.Log ("Registration done");
	}

	private static byte[] SerializeTile (object o)
	{
		BinaryFormatter bf = new BinaryFormatter ();
		MemoryStream ms = new MemoryStream ();
		bf.Serialize (ms, o);
		byte[] bytes = ms.ToArray ();
		//Debug.Log ("Serialized to " + bytes);
		return bytes;
	}
	
	private static object DeserializeTile (byte[] bytes)
	{
		BinaryFormatter bf = new BinaryFormatter ();
		MemoryStream ms = new MemoryStream (bytes);
		//Debug.Log ("Reading " + bytes);
		Tile tile = (Tile)bf.Deserialize (ms);
		//Debug.Log ("Produced " + tile);
		return tile;
	}
}
