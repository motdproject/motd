﻿using UnityEngine;
using System.Collections;

public class NetworkUtilities : MonoBehaviour
{
	[RPC]
	void ParentObject (string parentName)
	{
		transform.parent = GameObject.Find (parentName).transform;
	}
}
