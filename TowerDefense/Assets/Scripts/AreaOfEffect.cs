﻿using UnityEngine;
using System.Collections;

public class AreaOfEffect : MonoBehaviour
{
	public GameObject outline;
	public bool useOutline = true;

	public float Size {
		get {
			return _size;
		}
		set {
			_size = value;
			GetComponent<Projector> ().orthographicSize = _size;
			outline.GetComponent<Projector> ().orthographicSize = _size;
		}
	}

	public Color MainColor {
		get {
			return _mainColor;
		}
		set {
			_mainColor = value;

			Material copy = (Material)(Instantiate (GetComponent<Projector> ().material));
			copy.color = new Color (_mainColor.r, _mainColor.g, _mainColor.b, _mainColor.a * 0.7f);
			GetComponent<Projector> ().material = copy;
			
			Material copyOutline = (Material)(Instantiate (outline.GetComponent<Projector> ().material));
			copyOutline.color = _mainColor;
			outline.GetComponent<Projector> ().material = copyOutline;

		}
	}

	private Color _mainColor;
	private float _size;

	void Start ()
	{
		if (!useOutline) {
			outline.SetActive (false);
		}
	}
}
